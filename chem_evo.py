#!/usr/bin/env python3

# This code is written for python 3.6.5 with anaconda

import os
import sys
from matplotlib import colors
import matplotlib.ticker
import numpy as np

sys.path.insert(0, os.path.abspath(os.path.dirname(__file__)))
from divan import Divan, TablePlus
from matplotlib import pyplot as plt
import datetime

def chem_i_minus_j(ch1,ch2,*args, **kwargs):
    divan = Divan(**kwargs)
    chem_1 = TablePlus.TablePlus(divan.read_chemical_structure(divan.input_folder+'Chemistry_{:05}'.format(ch1)))
    chem_2 = TablePlus.TablePlus(divan.read_chemical_structure(divan.input_folder+'Chemistry_{:05}'.format(ch2)))

    divan.chemical_structure = chem_2 / chem_1

    divan.read_chemitry_totalmass()

    divan.generate_figure_most_total_abundant(
        Npages=40, image_kwargs={'cmap': 'RdYlGn'},
        normalizer=colors.LogNorm(vmin=1e-1, vmax=1e1),
        suptitle="Relative diff"
    )


    divan.save_figures_pdf(output_filename='evo.pdf')


if __name__ == '__main__':
    chem_i_minus_j(3,4)
