#!/usr/bin/env python3

# This code is written for python 3.7.0 with anaconda

import os
import sys
import logging

try:
    from divan import Divan, demo
except ImportError:
    logging.error("divan package was not found!"
                  "Setup it with:"
                  " $ python setup.py install [--user]"
                  "The script will continue...\a")
    sys.path.insert(0, os.path.abspath(os.path.dirname(__file__)))
    from divan import Divan
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Plot ANDES2 output')

    parser.add_argument("-i", "--input_folder", type=str,
                        help="Path to input", default="../data_output/")
    parser.add_argument("-c", "--full_chemistry", action="store_true",
                        help="Plot most of species (takes time!")
    parser.add_argument("-e", "--chem_evo", nargs=2, type=int,
                        help="Plot evolution of species (2 arguments) (takes time!")
    parser.add_argument("-v", "--verbosity", action="count",
                        help="increase output verbosity", default=1)
    parser.add_argument("-f", "--input_format", type=str,
                        help="Choose reader: [ANDES2], ANDES1", default='ANDES2')

    args = parser.parse_args()
    if args.chem_evo:
        demo.chem_i_minus_j(*args.chem_evo, **vars(args))
    else:
        demo.demo_full(**vars(args))
