#!/usr/bin/env python
from setuptools import setup
import sys
import logging

try:
    import matplotlib
except ImportError:
    logging.error("Matplotlib was not found in your environment! If you are using [ana]conda, you better start with \n"
          " $ conda install anaconda \n"
          "otherwise continue.")
    if sys.__stdin__.isatty():
        if input("Type 'yes' if you want to continue:") == 'yes':
            pass
        else:
            exit("Leaving installation.")

setup(
    name='divan',
    version='0.3',
    packages=['divan'],
    data_files=[('divan', ['divan/divan.mplstyle'])],
    url='https://gitlab.com/ANDES-dev/ANDES2/',
    python_requires='>=3.6',
    install_requires=[
        'numpy>1.10',
        'astropy>=3.0.0',
        'matplotlib>=3.0.2',
        'scipy>1.0.0',
        'tqdm'
    ],
    license='MIT',
    author='G.V. Smirnov-Pinchukov',
    author_email='smirnov@mpia.de',
    description='It is a sub-project of ANDES2 package, while it can be updated to read data in any format. '
                'DIVAN is designed to read and represent disk structure with object-oriented interface, '
                'based on matplotlib and astropy.table packages.'
)
