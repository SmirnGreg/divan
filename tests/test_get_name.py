from divan.names import get_name
import pytest
import astropy.table.column


@pytest.mark.parametrize(
    "input, expected",
    (
            ("hello", "hello"),
            (r"<H>", r"$\langle{}$H$\rangle{}$"),
            ("n(H+2H2)", r"$\langle{}$H$\rangle{}$"),
            (None, "None"),
            ("HCO+", "HCO$^+$"),
            ("H2", "H$_2$"),
            ("H3+", "H$_3^+$"),
            ("oH3+", "ortho-H$_3^+$"),
            ("pH2", "para-H$_2$"),
            ("H$_3^+$", "H$_3^+$"),
            ("H2D+", "H$_2$D$^+$"),
            ("gCO", "CO ice"),
            ("CO2 + H2", "CO$_2$ + H$_2$"),
            ("oH2 + pH2", "H$_2$"),
            ("oH3+ + pH3+", "H$_3^+$"),
            ("mD3+ + oD3+ + pD3+", "D$_3^+$"),
            ("oD3+ + pD3+ + mD3+", "D$_3^+$"),
            ("MG", "Mg"),
            ("MG+", "Mg$^+$"),
            ("NA+", "Na$^+$"),
            ("FE", "Fe"),
            ("SI", "Si"),
            ("Si", "Si"),
            ("HE", "He"),
            ("CL", "Cl"),
            ("game", "game"),
            ("oH2D+ + pH2D+ + 2 * oHD2+ + 2 * pHD2+ + 3 * oD3+ + 3 * pD3+ + 3 * mD3+", "D in H$_3^+$"),
            ("MG+ + NA+ + FE+", "M$^+$"),
            (astropy.table.column.Column([0, 1, 2], "arbitrary"), "arbitrary"),
    )
)
def test_get_name(input, expected):
    assert get_name(input) == expected
