from unittest import TestCase
from divan.interpolation import interpolate
import numpy as np


class TestInterpolate(TestCase):
    def test_interpolate_linear_input(self):
        X = np.linspace(0, 1, 2)
        Y = np.linspace(0, 1, 2)
        x, y = np.meshgrid(X, Y)
        x = x.flatten()
        y = y.flatten()
        data = x + y
        x_out = [0.5]
        y_out = [0.5]
        np.testing.assert_allclose(
            interpolate(x, y, data, x_out, y_out, kx=1, ky=1),
            1,
            rtol=0.01
        )

    def test_interpolate_not_ndarray(self):
        X = np.linspace(0, 1, 2)
        Y = np.linspace(0, 1, 2)
        x, y = np.meshgrid(X, Y)
        x = x.flatten()
        y = y.flatten()
        data = x + y
        x_out = 0.5
        y_out = 0.5
        np.testing.assert_allclose(
            interpolate(x, y, data, x_out, y_out, kx=1, ky=1),
            1,
            rtol=0.01
        )

    def test_interpolation_for_given_callables(self):
        callables = [
            lambda x, y: x + y,
            lambda x, y: x * x + y * y,
            lambda x, y: np.sin(3 * x) * np.sin(3 * y)
        ]
        X = np.linspace(0, 1, 40)
        Y = np.linspace(0, 1, 40)
        x, y = np.meshgrid(X, Y)
        x = x.flatten()
        y = y.flatten()
        x_out = np.array([0.03, 0.07, 0.890, 0.921])
        y_out = np.array([0.01, 0.76, 0.21, 0.962])
        for call in callables:
            with self.subTest(call=call):
                np.testing.assert_allclose(
                    interpolate(x, y, call(x, y), x_out, y_out),
                    call(x_out, y_out),
                    rtol=0.05, atol=0.05
                )
