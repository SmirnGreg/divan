from divan import TablePlus
import pytest
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.colors import LogNorm


@pytest.mark.parametrize("method", ['__add__', '__sub__', '__mul__', '__truediv__'])
def test_arithmetic(method):
    size = 5

    x = np.linspace(1, 10, size)
    y = np.geomspace(1, 10, size)

    a1 = np.random.random_sample(size)
    a2 = np.random.random_sample(size)
    b1 = np.random.random_sample(size)
    b2 = np.random.random_sample(size)
    c1 = np.random.random_sample(size)
    c2 = np.random.random_sample(size)

    tbl1 = TablePlus()
    tbl1['x'] = x
    tbl1['y'] = y
    tbl1['a'] = a1
    tbl1['b'] = b1
    tbl1['c'] = c1

    tbl2 = TablePlus()
    tbl2['x'] = x
    tbl2['y'] = y
    tbl2['a'] = a2
    tbl2['b'] = b2
    tbl2['c'] = c2

    tbl = tbl1.__getattribute__(method)(tbl2)
    np.testing.assert_allclose(tbl1['x'], tbl['x'])
    np.testing.assert_allclose(tbl1['y'], tbl['y'])
    np.testing.assert_allclose(tbl1['a'].__getattribute__(method)(tbl2['a']), tbl['a'])
    np.testing.assert_allclose(tbl1['b'].__getattribute__(method)(tbl2['b']), tbl['b'])
    np.testing.assert_allclose(tbl1['c'].__getattribute__(method)(tbl2['c']), tbl['c'])


@pytest.mark.parametrize("method", ['__add__', '__sub__', '__mul__', '__truediv__'])
def test_failure_different_coords(method):
    size = 5

    x1 = np.linspace(1, 10, size)
    y1 = np.geomspace(1, 10, size)
    x2 = np.logspace(1, 10, size)
    y2 = np.linspace(1, 10, size)

    a1 = np.random.random_sample(size)
    a2 = np.random.random_sample(size)
    b1 = np.random.random_sample(size)
    b2 = np.random.random_sample(size)
    c1 = np.random.random_sample(size)
    c2 = np.random.random_sample(size)

    tbl1 = TablePlus()
    tbl1['x'] = x1
    tbl1['y'] = y1
    tbl1['a'] = a1
    tbl1['b'] = b1
    tbl1['c'] = c1

    tbl2 = TablePlus()
    tbl2['x'] = x2
    tbl2['y'] = y2
    tbl2['a'] = a2
    tbl2['b'] = b2
    tbl2['c'] = c2

    with pytest.raises(TypeError):
        tbl1.__getattribute__(method)(tbl2)


@pytest.mark.parametrize("method", ['__add__', '__sub__', '__mul__', '__truediv__'])
def test_failure_different_length(method):
    size1 = 5
    size2 = 2 * size1
    x1 = np.linspace(1, 10, size1)
    y1 = np.geomspace(1, 10, size1)
    x2 = np.hstack((x1, x1))
    y2 = np.hstack((y1, y1))

    a1 = np.random.random_sample(size1)
    a2 = np.random.random_sample(size2)
    b1 = np.random.random_sample(size1)
    b2 = np.random.random_sample(size2)
    c1 = np.random.random_sample(size1)
    c2 = np.random.random_sample(size2)

    tbl1 = TablePlus()
    tbl1['x'] = x1
    tbl1['y'] = y1
    tbl1['a'] = a1
    tbl1['b'] = b1
    tbl1['c'] = c1

    tbl2 = TablePlus()
    tbl2['x'] = x2
    tbl2['y'] = y2
    tbl2['a'] = a2
    tbl2['b'] = b2
    tbl2['c'] = c2

    with pytest.raises(TypeError):
        tbl1.__getattribute__(method)(tbl2)


def test_interpolate(do_plots=False):
    # do_plots = True
    foo_a = lambda x, y: (x - 5) ** 2 + (y - 4) ** 3
    foo_b = lambda x, y: 40 * np.exp(-(x - 5) ** 2 - (y - 4) ** 2)
    np.random.seed(100)
    size1 = 200
    size2 = 5000
    x1 = np.random.sample(size1) * 10
    y1 = np.random.sample(size1) * 10
    x2 = np.random.sample(size2) * 10
    y2 = np.random.sample(size2) * 10

    a1 = foo_a(x1, y1)
    a2 = foo_a(x2, y2)
    b1 = foo_b(x1, y1)
    b2 = foo_b(x2, y2)

    tbl_low = TablePlus()
    tbl_low['x'] = x1
    tbl_low['y'] = y1
    tbl_low['a'] = a1
    tbl_low['b'] = b1

    tbl_high = TablePlus()
    tbl_high['x'] = x2
    tbl_high['y'] = y2
    tbl_high['a'] = a2
    tbl_high['b'] = b2

    tbl_lowered = tbl_high.interpolate(tbl_low)
    tbl_diff = tbl_low - tbl_lowered
    tbl_ratio = tbl_low / tbl_lowered

    if do_plots:
        fig, ax = plt.subplots(2, 5, sharex=True, sharey=True, figsize=(13, 5))
        ax[0, 0].tripcolor(tbl_low['x'], tbl_low['y'], tbl_low['a'], vmin=0, vmax=40)
        ax[0, -1].tripcolor(tbl_high['x'], tbl_high['y'], tbl_high['a'], vmin=0, vmax=40)
        ax[0, -2].tripcolor(tbl_lowered['x'], tbl_lowered['y'], tbl_lowered['a'], vmin=0, vmax=40)
        ax[0, 1].tripcolor(tbl_diff['x'], tbl_diff['y'], tbl_diff['a'], vmin=-1, vmax=1, cmap="coolwarm")
        ax[0, 2].tripcolor(tbl_ratio['x'], tbl_ratio['y'], tbl_ratio['a'], norm=LogNorm(vmin=0.5, vmax=2),
                           cmap="coolwarm")
        ax[1, 0].tripcolor(tbl_low['x'], tbl_low['y'], tbl_low['b'], vmin=0, vmax=40)
        ax[1, -1].tripcolor(tbl_high['x'], tbl_high['y'], tbl_high['b'], vmin=0, vmax=40)
        ax[1, -2].tripcolor(tbl_lowered['x'], tbl_lowered['y'], tbl_lowered['b'], vmin=0, vmax=40)
        ax[1, 1].tripcolor(tbl_diff['x'], tbl_diff['y'], tbl_diff['b'], vmin=-1, vmax=1, cmap="coolwarm")
        ax[1, 2].tripcolor(tbl_ratio['x'], tbl_ratio['y'], tbl_ratio['b'], norm=LogNorm(vmin=0.5, vmax=2),
                           cmap="coolwarm")
        ax[0, 0].set_title("Original low-res")
        ax[0, 1].set_title("low - lowered")
        ax[0, 2].set_title("low / lowered")
        ax[0, -2].set_title("Lowered high-res")
        ax[0, -1].set_title("Original high-res")
        fig.savefig("test_interpolation.png")

    for column in tbl_low.columns[0:2]:
        np.testing.assert_allclose(tbl_lowered[column], tbl_low[column], rtol=1e-7)
    for column in tbl_low.columns[2:]:
        tbl_low[column][np.where(np.isnan(tbl_lowered[column]))] = np.nan
        np.testing.assert_allclose(tbl_lowered[column], tbl_low[column], atol=0.05, rtol=0.1)


def test_different_columns():
    foo_a = lambda x, y: (x - 5) ** 2 + (y - 4) ** 3
    foo_b = lambda x, y: 40 * np.exp(-(x - 5) ** 2 - (y - 4) ** 2)
    np.random.seed(100)
    size1 = 200
    size2 = 5000
    x1 = np.random.sample(size1) * 10
    y1 = np.random.sample(size1) * 10
    x2 = np.random.sample(size2) * 10
    y2 = np.random.sample(size2) * 10

    a1 = foo_a(x1, y1)
    a2 = foo_a(x2, y2)
    b1 = foo_b(x1, y1)
    b2 = foo_b(x2, y2)

    tbl_low = TablePlus()
    tbl_low['x'] = x1
    tbl_low['y'] = y1
    tbl_low['a'] = a1
    # No 'b' column in tbl_low

    tbl_high = TablePlus()
    tbl_high['x'] = x2
    tbl_high['y'] = y2
    tbl_high['a'] = a2
    tbl_high['b'] = b2

    tbl_lowered = tbl_high.interpolate(tbl_low)
    tbl_highered = tbl_low.interpolate(tbl_high)

    for column in tbl_low.columns[0:2]:
        np.testing.assert_allclose(tbl_lowered[column], tbl_low[column], rtol=1e-7)
    for column in tbl_low.columns[2:]:
        tbl_low[column][np.where(np.isnan(tbl_lowered[column]))] = np.nan
        np.testing.assert_allclose(tbl_lowered[column], tbl_low[column], atol=0.05, rtol=0.1)
    # TODO accurately do this test for both lowering and highering
    # for column in tbl_high.columns[0:2]:
    #     np.testing.assert_allclose(tbl_highered[column], tbl_high[column], rtol=1e-7)
    # for column in tbl_high.columns[2:]:
    #     tbl_low[column][np.where(np.isnan(tbl_lowered[column]))]=np.nan
    #     np.testing.assert_allclose(tbl_lowered[column], tbl_low[column], atol=0.05, rtol=0.1)


@pytest.mark.parametrize(
    "input, expected",
    (
            ("CO", {"CO": 1}),
            ("DCO+ + HCO+", {"DCO+": 1, "HCO+": 1}),
            ("DCO+ + HCO+ + HCO+", {"DCO+": 1, "HCO+": 2}),
            ("DCO+ + 2 * HCO+", {"DCO+": 1, "HCO+": 2}),
    )
)
def test_resolve_key_formula(input, expected):
    assert TablePlus.resolve_key_formula(input) == expected


TABLE = TablePlus([[2, 3, 4], [1, 2, 5]], names=["CO", "H2"])


@pytest.mark.parametrize(
    "input, expected",
    (
            ("CO", [2, 3, 4]),
            ("CO + H2", [3, 5, 9]),
            ("CO + 2 * H2", [4, 7, 14]),
            ("CO / H2", [2., 1.5, 0.8]),
            ("CO / CO + H2", [2. / 3, 3. / 5, 4. / 9]),
    )
)
def test_get_table_with_formula(input, expected):
    out = TABLE[input]
    assert np.allclose(out, expected)
    assert out.name == input


def test_slice():
    assert np.all(TABLE[:] == TABLE)