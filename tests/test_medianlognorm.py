import numpy as np

from divan import MedianLogNorm


def test_medianlognorm():
    dat = np.arange(10) + 1
    norm = MedianLogNorm(median=3)
    np.testing.assert_allclose(
        norm(dat),
        np.array([0.0, 0.3154648767857287, 0.5, 0.6194719977968457, 0.7121416787532774,
                  0.7878583212467224, 0.8518758303106261, 0.9073303190435682,
                  0.9562446446965992, 1.0])
    )