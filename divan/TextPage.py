from matplotlib import pyplot as plt


class TextPage:
    """MixIn class for LaTeX and text output"""

    def generate_text_page(
            self,
            text_to_write=r'''
            I am a table: \newline \newline
            \begin{tabular}{ l | l } 
            Parameter   & Value \\ \hline 
            Mass        & $10\ M_\odot$  \\ 
            Temperature & 12 K  \\
            \end{tabular} \newline \newline 
            use text\_to\_write=r'xxx' to change me.''',
            suptitle=r'I am \LaTeX{} and can render $a=b^2$. Change me with suptitle=r"xxx"',
    ):
        r"""
        Method to generate a text page, supports LaTeX.

        This method returns figure with generated image, replaces self.figure_text with it and appends it to self.figures list.

        :param text_to_write: string containg LaTeX text. \n will be replaced with spaces, use \\ for newlines
        :param suptitle: page title
        :return: matplotlib.figure.Figure with a text page generated
        """
        self.figure_text = plt.figure()
        self.figure_text.suptitle(suptitle)
        no_newline_text_to_write = text_to_write.replace('\n', ' ')
        self.figure_text.text(0.5, 0.5, no_newline_text_to_write, verticalalignment='center',
                              horizontalalignment='center')
        self.figures.append(self.figure_text)
        return self.figure_text
