from divan import Divan, TablePlus
import matplotlib.colors
import datetime
import numpy as np
import logging


def demo_full(plot_full_chemistry=False, *args, **kwargs):
    chemistry_kwargs = {
        # 'cmap': 'gist_ncar',
    }
    # Normalizer for some future images
    chem_normalizer = matplotlib.colors.LogNorm(vmin=1e-7, vmax=1e-2)

    # Initialize default Divan instance
    divan = Divan(**kwargs)

    # Self-explaining, read structures from default location and store in attributes of divan
    divan.read_surface_densities()
    divan.read_physical_structure()
    divan.read_chemical_structure()
    divan.read_chemitry_totalmass()
    divan.read_gtb()

    logging.info("Elapsed time for reading: ", datetime.datetime.now() - divan.inittime)

    # Generate text page with hints on how to fill it
    divan.generate_text_page()
    # Generate surface density plot
    divan.generate_figure_surface_densities()
    # Generate volume distance plot, interpolate it on 100x100 grid with linear method, overplot with given levels,
    # multiply dust panel by 100
    divan.generate_figure_volume_densities(
        do_interpolate=100, interpolation_kwargs={'method': 'linear'},
        levels=[1e-11, 1e-12, 1e-13, 1e-14, 1e-15, 1e-16, 1e-17, 1e-18],
        extra_gas_to_dust=100
    )
    # Add extra contours on latest figure_volume_densities, axes[0] -> main image, not contour
    # Try to find column 'AV_true' first in divan.physical_structure, then in divan.chemical_structure
    # Plot given levels, with white color, label with Av = .., rendered as LaTeX formula,
    # Interpolate contours on 100x100 grid with cubic method
    divan.add_extra_contours(
        divan.figure_volume_densities.axes[0],
        contour_data_columnname='AV_true',
        levels=[1, 10, 100],
        contour_kwargs={'colors': 'white'},
        clabel=r'$A_V = %d$',
        do_interpolate=100, interpolation_kwargs={'method': 'cubic'},
    )

    # Generate figure with temperatures, use 'plasma' colormap for image, 'Oranges' for contours
    divan.generate_figure_temperatures(
        do_interpolate=100, interpolation_kwargs={'method': 'linear'},
        image_kwargs={'cmap': 'plasma'},
        contours_kwargs={'cmap': 'Oranges'},
        levels=[10, 30, 100, 300, 1000],
    )
    # Add contours on latest figure from the whole list (divan.figures[1]), main axes[0]
    # interpolate_logarithms -> interpolate logarithms of data instead of data itself
    divan.add_extra_contours(
        divan.figures[-1].axes[0],
        contour_data_columnname='gH2O',
        levels=[1e-6],
        contour_kwargs={'colors': '#b4cffa'},
        clabel=r'$n_{gH_2O}/n_{H} = %5.2e$',
        do_interpolate=100, interpolation_kwargs={'method': 'cubic'}, interpolate_logarithms=True
    )

    if plot_full_chemistry:
        # Make 10 pages of of most abundant species with nipy_spectral colormap and white under_color,
        # with data range from 1 to 1e10 on top, and the same data multiplied by 1e10 on bottom subimages
        divan.generate_figure_most_total_abundant(
            Npages=10, under_color='w', image_kwargs={'cmap': 'nipy_spectral'},
            normalizer=matplotlib.colors.LogNorm(vmin=1e0, vmax=1e10), bottom_data_multiplier=1e10,
            suptitle="Absolute abundances"  # , colorbar_ticks=matplotlib.ticker.MaxNLocator(7)
        )
        # And 40 more pages of low abundant species
        divan.generate_figure_most_total_abundant(
            Npages=40, under_color='w', image_kwargs={'cmap': 'nipy_spectral'},
            normalizer=matplotlib.colors.LogNorm(vmin=1e-5, vmax=1e5), bottom_data_multiplier=1e10,
            suptitle="Absolute abundances"
        )
        divan.skip_pages_most_total_abundant = 0
        # Revert to 0 page: new .generate_figure_most_total_abundant() will plot from the 1st species again

    # Convert divan.chemical_structure table to relative abundances from absolute abundances
    divan.normalize_chemical_structure()
    # Now abundances are relative! To <H> by default

    # Generate levels
    level = list(np.power(10., np.arange(-8, -2, 1)))

    divan.generate_figure_chemistry(
        spec1='H2O', spec2='gH2O',
        do_interpolate=100, interpolation_kwargs={'method': 'linear'},
        normalizer=chem_normalizer,
        levels=level,
        image_kwargs=chemistry_kwargs, under_color='#ffffff'
    )

    divan.basic_chemistry_visualization(
        normalizer=matplotlib.colors.LogNorm(vmin=1e-3, vmax=1), bottom_data_multiplier=1000, under_color='papayawhip')
    divan.basic_chemistry_visualization(
        normalizer=matplotlib.colors.LogNorm(vmin=1e-9, vmax=1e-3),
        first_plot_species=('H2O', 'OH', 'CO2', 'CO'),
        image_kwargs={'cmap': 'cubehelix'}, under_color='w'
    )

    if plot_full_chemistry:
        divan.generate_figure_most_total_abundant(
            Npages=4, normalizer=matplotlib.colors.LogNorm(vmin=1e-6, vmax=1), under_color='w',
            bottom_data_multiplier=1e6, image_kwargs={'cmap': 'nipy_spectral'}, cbar_kwargs={'extend': 'both'},
        )
        divan.generate_figure_most_total_abundant(
            Npages=4, normalizer=matplotlib.colors.LogNorm(vmin=1e-9, vmax=1e-3), under_color='w',
            bottom_data_multiplier=1e6, image_kwargs={'cmap': 'nipy_spectral'}, cbar_kwargs={'extend': 'both'},
        )
        divan.generate_figure_most_total_abundant(
            Npages=20, normalizer=matplotlib.colors.LogNorm(vmin=1e-12, vmax=1e-6), under_color='w',
            bottom_data_multiplier=1e6, image_kwargs={'cmap': 'nipy_spectral'}, cbar_kwargs={'extend': 'both'},
        )

    divan.generate_figure_gtb(do_interpolate=100) # TODO why does not work w/p interpolation
    divan.add_extra_contours(
        divan.figures[-1].axes[0],
        contour_data_columnname='AV_true',
        levels=[1, 10, 100],
        contour_kwargs={'colors': 'black'},
        clabel=r'$A_V = %d$',
        do_interpolate=100, interpolation_kwargs={'method': 'cubic'},
        both_panels=False
    )
    divan.generate_figure_most_active_coolant_or_heater()
    divan.add_extra_contours(
        divan.figures[-1].axes[0],
        contour_data_columnname='AV_true',
        levels=[1, 10, 100],
        contour_kwargs={'colors': 'white'},
        clabel=r'$A_V = %d$',
        do_interpolate=100, interpolation_kwargs={'method': 'cubic'},
        both_panels=False
    )
    divan.generate_figure_most_active_coolant_or_heater(heaters=True)
    divan.add_extra_contours(
        divan.figures[-1].axes[0],
        contour_data_columnname='AV_true',
        levels=[1, 10, 100],
        contour_kwargs={'colors': 'white'},
        clabel=r'$A_V = %d$',
        do_interpolate=100, interpolation_kwargs={'method': 'cubic'},
        both_panels=False
    )
    # for all species
    divan.generate_most_abundant_species()
    divan.generate_figure_most_abundant_species()

    # divan.generate_figure_temperatures(
    #     r=divan.gtb['Radius'], z=divan.gtb['Height'], dust_temperature=divan.gtb['Dust temperature'],
    #     gas_temperature=divan.gtb['Tgas from GTB'],
    #     do_interpolate=100, interpolation_kwargs={'method': 'linear'},
    #     image_kwargs={'cmap': 'plasma'},
    #     contours_kwargs={'cmap': 'Oranges'},
    #     levels=[10, 20, 40, 100, 200, 400, 1000, 2000, 4000, 10000], )


    # divan.generate_figure_most_abundant_species(most_abundant=divan.gtb["dominant_heater_name"])
    # divan.generate_figure_most_abundant_species(most_abundant=divan.gtb["dominant_coolant_name"])
    # also valid syntax
    # Plot only if number density is >1e-10 cm-3, only from given species
    divan.generate_most_abundant_species(('CO', 'CO2', 'H2O'), threshold=1e-10, threshold_on_absolute=True)
    # any valid matplotlib color notation
    divan.generate_figure_most_abundant_species(
        colors_for_species=('r', 'chocolate', '#d0a340'))

    # or even like this:
    # renormalize chemical structure to H2O
    divan.normalize_chemical_structure('H2O')
    # Plot only if abundance to current species is >1e-10
    divan.generate_figure_most_abundant_species(species=('H2O', 'gH2O', 'OH', 'gOH'), threshold=1e-10)

    # report current status to stdout:
    print(divan)

    # save to pdf
    divan.save_figures_pdf()  # a4=True)

    print("Elapsed time: ", datetime.datetime.now() - divan.inittime)


def chem_i_minus_j(ch1, ch2, *args, **kwargs):
    divan = Divan(**kwargs)
    chem_1 = TablePlus.TablePlus(divan.read_chemical_structure(divan.input_folder + 'Chemistry_{:05}'.format(ch1)))
    chem_2 = TablePlus.TablePlus(divan.read_chemical_structure(divan.input_folder + 'Chemistry_{:05}'.format(ch2)))

    divan.chemical_structure = chem_2 / chem_1

    divan.read_chemitry_totalmass()

    divan.generate_figure_most_total_abundant(
        Npages=40, image_kwargs={'cmap': 'RdYlGn'},
        normalizer=matplotlib.colors.LogNorm(vmin=1e-1, vmax=1e1),
        suptitle="Relative diff"
    )

    divan.save_figures_pdf(output_filename='evo.pdf')
