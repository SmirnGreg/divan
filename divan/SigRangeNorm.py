import matplotlib.colors
import numpy as np


class SigRangeNorm(matplotlib.colors.Normalize):
    """
    Normalize a given value to the 0-1 range on a log scale, with 0.5 equals to median
    """

    def __init__(self, sigrange=0.999, *args, **kwargs):
        super(SigRangeNorm, self).__init__(*args, **kwargs)
        self.sigrange = sigrange

    def autoscale_None(self, A):
        """autoscale only None-valued vmin or vmax."""
        A = np.asanyarray(A)
        if self.vmin is None and A.size:
            self.vmin = np.nanpercentile(np.array(A), 0.5 * (1 - self.sigrange) * 100)
        if self.vmax is None and A.size:
            self.vmax = np.nanpercentile(np.array(A), (1 - 0.5 * self.sigrange) * 100)
