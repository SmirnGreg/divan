from copy import copy

import scipy.interpolate
import numpy as np


def interpolate(
        x, y, data,
        x_out=None, y_out=None,
        x_out_size=None, y_out_size=None,
        interpolate_logarithms=False,
        do_interpolation=True,
        verbosity=0, grid=False,
        **kwargs
):
    if not do_interpolation:
        return data

    if x_out is None:
        if x_out_size is None:
            x_out = x
        else:
            x_out = np.linspace(min(x), max(x), x_out_size)
    if y_out is None:
        if y_out_size is None:
            y_out = y
        else:
            y_out = np.linspace(min(y), max(y), y_out_size)

    if not isinstance(x_out, np.ndarray):
        x_out = np.array(x_out)
    if not isinstance(y_out, np.ndarray):
        y_out = np.array(y_out)

    if interpolate_logarithms:
        data_local = np.log10(data)
    else:
        data_local = copy(data)

    data_out = scipy.interpolate.griddata(np.vstack((x, y)).T, data_local, np.vstack((x_out, y_out)).T)
    if interpolate_logarithms:
        data_out = np.power(10, data_out)
    return data_out
