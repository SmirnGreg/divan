import logging
import copy
import collections

from matplotlib import pyplot as plt
import matplotlib.colors, matplotlib.cm
import astropy.table
import numpy as np

from divan.plot_functions import generate_2d_disk_figure
from divan.MedianLogNorm import MedianLogNorm
from divan._DivanBase import DivanBase

module_logger = logging.getLogger("divan.2d.physics")


class Physics(DivanBase):
    """
    MixIn class for physics visualisation
    """

    def generate_figure_volume_densities(
            self,
            r=None, z=None,
            dust_density=None, gas_density=None,
            do_interpolate=None,  # Points to interpolate, no interpolation if False
            interpolation_kwargs=None,
            contours_kwargs=None,
            image_kwargs=None,
            levels=None,
            extra_gas_to_dust=1,  # Value to multiply by dust density before plotting
            title="Gas and dust density",
            colorbar_label=r'Volume density [g cm$^{-3}$]',
            **kwargs
    ):
        """
        This method returns figure with generated image, replaces self.figure_volume_densities with it and appends it to self.figures list.

        :param r:
        :param z:
        :param dust_density:
        :param gas_density:
        :param do_interpolate:
        :param interpolation_kwargs:
        :param contours_kwargs:
        :param image_kwargs:
        :param levels:
        :param extra_gas_to_dust:
        :return:
        """
        interpolation_kwargs = interpolation_kwargs or {}
        contours_kwargs = contours_kwargs or {}
        image_kwargs = image_kwargs or {}
        if 'cmap' not in image_kwargs.keys():
            image_kwargs['cmap'] = matplotlib.cm.get_cmap('PuBuGn', 10)
        try:
            levels_sorted = sorted(levels)
        except TypeError:
            levels_sorted = None

        if (r is None) or (z is None) or (dust_density is None) or (gas_density is None):
            try:
                r, z, dust_density, gas_density = (
                    self.physical_structure['Radius'],
                    self.physical_structure['Height'],
                    self.physical_structure['Dust density'],
                    self.physical_structure['Gas density']
                )
                module_logger.info("Parameters successfully taken from self")
            except:  # TODO make better exception
                raise AttributeError(
                    'No data given for generate_figure_volume_densities(). Use read_physical_structure() or send arguments')
        self.figure_volume_densities, ax_volume_densities = plt.subplots()
        if self.titles: ax_volume_densities.set_title(title)
        ax_volume_densities.set_xlabel('Radius [au]')
        ax_volume_densities.set_ylabel('Height/Radius')

        fig = generate_2d_disk_figure(
            r, z, top_panel_data=gas_density, top_panel_levels=levels_sorted,
            bottom_panel_data=dust_density,
            bottom_data_multiplier=extra_gas_to_dust, top_image_kwargs=image_kwargs,
            top_interpolate=do_interpolate, axes=ax_volume_densities, colorbar_label=colorbar_label,
            top_interpolation_kwargs=interpolation_kwargs, top_contours_kwargs=contours_kwargs,
            top_interpolate_logarithms=True, **kwargs
        )
        self.figures.append(fig)
        return fig

    def generate_figure_temperatures(
            self,
            r=None, z=None,
            dust_temperature=None, gas_temperature=None,
            do_interpolate=None,  # Points to interpolate, no interpolation if False
            interpolation_kwargs=None,
            contours_kwargs=None,
            image_kwargs=None,
            levels=None,
            colorbar_label=r'Temperature [K]',
            **kwargs
    ):
        """
        This method returns figure with generated image, replaces self.figure_temperatures with it and appends it to self.figures list.

        :param r:
        :param z:
        :param dust_temperature:
        :param gas_temperature:
        :param do_interpolate:
        :param interpolation_kwargs:
        :param contours_kwargs:
        :param image_kwargs:
        :param levels:
        :return:
        """
        interpolation_kwargs = interpolation_kwargs or {}
        contours_kwargs = contours_kwargs or {}
        image_kwargs = image_kwargs or {}
        if 'cmap' not in image_kwargs.keys():
            image_kwargs['cmap'] = matplotlib.cm.get_cmap('afmhot', 10)
        try:
            levels_sorted = sorted(levels)
        except TypeError:
            levels_sorted = None
        module_logger.info("Generating figure of temperatures")
        if (r is None) or (z is None) or (dust_temperature is None) or (gas_temperature is None):
            try:
                r, z, dust_temperature, gas_temperature = (
                    self.physical_structure['Radius'],
                    self.physical_structure['Height'],
                    self.physical_structure['Dust temperature'],
                    self.physical_structure['Gas temperature']
                )
                module_logger.info("Parameters successfully taken from self")
            except:  # TODO make better exception
                raise AttributeError(
                    'No data given for generate_figure_volume_densities(). Use read_physical_structure() or send arguments')
        self.figure_temperatures, ax_temperatures = plt.subplots()
        if self.titles: ax_temperatures.set_title('Gas and dust temperature')
        ax_temperatures.set_xlabel('Radius [au]')
        ax_temperatures.set_ylabel('Height/Radius')

        fig = generate_2d_disk_figure(
            r, z, top_panel_data=gas_temperature, top_panel_levels=levels_sorted,
            bottom_panel_data=dust_temperature,
            top_image_kwargs=image_kwargs,
            top_contours_kwargs=contours_kwargs,
            top_interpolation_kwargs=interpolation_kwargs,
            top_interpolate=do_interpolate, axes=ax_temperatures, colorbar_label=colorbar_label,
            **kwargs
        )
        self.figures.append(fig)
        return fig

    def generate_figure_gtb(
            self,
            r=None, z=None,
            dust_temperature=None, gas_temperature=None,
            do_interpolate=None,  # Points to interpolate, no interpolation if False
            interpolation_kwargs=None,
            contours_kwargs=None,
            image_kwargs=None,
            levels=None,
            **kwargs
    ):
        """
        This method returns figure with generated image, replaces self.figure_temperatures with it and appends it to self.figures list.

        :param r:
        :param z:
        :param dust_temperature:
        :param gas_temperature:
        :param do_interpolate:
        :param interpolation_kwargs:
        :param contours_kwargs:
        :param image_kwargs:
        :param levels:
        :return:
        """

        interpolation_kwargs = interpolation_kwargs or {}
        contours_kwargs = contours_kwargs or {}
        image_kwargs = image_kwargs or {}
        if 'cmap' not in image_kwargs:
            image_kwargs['cmap'] = 'coolwarm'
        try:
            levels_sorted = sorted(levels)
        except TypeError:
            levels_sorted = None
        module_logger.info("Generating figure of gas thermal balance")
        if (r is None) or (z is None) or (dust_temperature is None) or (gas_temperature is None):
            try:
                r, z, dust_temperature, gas_temperature = (
                    self.gtb['Radius'],
                    self.gtb['Height'],
                    self.gtb['Dust temperature'],
                    self.gtb['Tgas from GTB']
                )
                module_logger.info("Parameters successfully taken from self")
            except:  # TODO make better exception
                raise AttributeError(
                    'No data given for generate_figure_gtb(). Use read_gtb() or send arguments')
        self.figure_temperatures, ax_temperatures = plt.subplots()
        if self.titles: ax_temperatures.set_title('Gas and dust temperature')
        ax_temperatures.set_xlabel('Radius [au]')
        ax_temperatures.set_ylabel('Height/Radius')
        ax_temperatures.autoscale(enable=True, axis='x', tight=True)
        ax_temperatures.autoscale(enable=True, axis='y', tight=True)
        fig = generate_2d_disk_figure(
            r, z, top_panel_data=gas_temperature / dust_temperature, top_panel_levels=levels_sorted,
            # bottom_panel_data=dust_temperature,
            top_image_kwargs=image_kwargs,
            top_contours_kwargs=contours_kwargs,
            top_interpolation_kwargs=interpolation_kwargs,
            top_interpolate=do_interpolate, axes=ax_temperatures, colorbar_label=r'Gas temperature / Dust temperature',
            normalizer=MedianLogNorm(vmin=8e-1, vmax=1e2, median=1.),
            **kwargs,
        )
        self.figures.append(fig)
        return fig

    def generate_figure_most_active_coolant_or_heater(
            self,
            r=None, z=None,
            most_active=None,
            image_kwargs=None,
            do_interpolate=100,
            interpolation_kwargs=None,
            colors_for_species=(),  # An iterable with any valid matplotlib color
            heaters=False,  # coolants by default
            cut_transition=False,
            cut_to_first_underscore=True,
            order=None,
            **kwargs
    ):
        """
        This method returns figure with generated image and appends it to self.figures list.

        :param r:
        :param z:
        :param most_active:
        :param image_kwargs:
        :param do_interpolate:
        :param interpolation_kwargs:
        :param colors_for_species:
        :param species:
        :param most_abundant_kwargs:
        :return:
        """
        if heaters:
            label = 'heater'
        else:
            label = 'coolant'

        interpolation_kwargs = interpolation_kwargs or {}
        image_kwargs = image_kwargs or {}

        maxcol = 10  # Todo depending on number of active species
        if (1 <= maxcol <= 10):
            tabname = 'tab10'
        else:
            tabname = 'tab20'
        tab10 = matplotlib.cm.get_cmap(tabname)
        colors_for_species = colors_for_species or tuple(map(tab10, np.linspace(0, 1, maxcol)))

        if (r is None) or (z is None):
            try:
                r, z = (
                    self.gtb['Radius'],
                    self.gtb['Height'],
                )
            except:
                raise AttributeError(
                    'No coordinates given for .generate_figure_most_active_coolant_or_heater(). '
                    'Use .read_gtb() or send arguments'
                )
        if most_active is None:
            try:
                if heaters:
                    most_active = self.gtb['dominant_heater_name']
                else:
                    most_active = self.gtb['dominant_coolant_name']
            except:
                raise AttributeError(
                    'No data given for .generate_figure_most_active_coolant_or_heater(). '
                    'Use .read_gtb() or send arguments'
                )
        figure_most_active, ax_most_active = plt.subplots()
        if self.titles:
            ax_most_active.set_title(
                'Most active {}'.format(label))
        ax_most_active.set_xlabel('Radius [au]')
        ax_most_active.set_ylabel('Height/Radius')
        ax_most_active.autoscale(enable=True, axis='x', tight=True)
        ax_most_active.autoscale(enable=True, axis='y', tight=True)

        most_active = copy.copy(most_active)
        for i, name in enumerate(most_active):
            name = str(name)
            if ">" in name and cut_transition:
                name = name.rpartition(">")[0].rpartition("_")[0]
            name = name.replace("->", r"$\rightarrow$")
            if cut_to_first_underscore:
                name = name.partition("_")[2]
            most_active[i] = name

        # if self.threshold_most_abundant_species_on_absolute:
        #     quan = r'n'
        #     dim = r'\ \mbox{cm}^{-3}'
        # else:
        #     quan = r'n/n_{\mbox{\scriptsize{%s}}}' % self.chemistry_normalize_to
        #     dim = ''
        most_abundant_odict = collections.OrderedDict(collections.Counter(most_active).most_common(maxcol))
        if order is not None:
            for i, mechanism in enumerate(order):
                if mechanism in most_abundant_odict.keys():
                    most_abundant_odict.move_to_end(mechanism, last=False)

        most_abundant_value = astropy.table.Column(
            length=len(most_active), dtype='int64',
            name=' '
            # name=r'$' + quan + '>' + str(self.threshold_most_abundant_species) + dim + r'$'
        )
        most_abundant_value[np.where(most_active == '')] = -1
        if '' in most_abundant_odict.keys():
            del (most_abundant_odict[''])
        for i, entry in enumerate(most_abundant_odict):
            most_abundant_value[np.where(most_active == entry)] = i

        cmap = matplotlib.colors.ListedColormap(colors_for_species[0:len(most_abundant_odict)])

        if 'cmap' not in image_kwargs.keys():
            cmap.set_under('w')
            image_kwargs['cmap'] = cmap  # 'tab10'
        label_cap = label
        label_cap.capitalize()
        self.figure_most_abundant_species = generate_2d_disk_figure(
            r, z, top_panel_data=most_abundant_value,
            top_image_kwargs=image_kwargs,
            top_interpolation_kwargs=interpolation_kwargs,
            top_interpolate=do_interpolate, axes=ax_most_active, colorbar_label=label_cap + 's',
            normalizer=matplotlib.colors.Normalize(vmin=-0.5, vmax=len(most_abundant_odict) - 0.5),
            colorbar_ticks=np.arange(len(most_abundant_odict)), colorbar_ticklabels=most_abundant_odict.keys(),
            **kwargs
        )
        self.figures.append(self.figure_most_abundant_species)

        return self.figure_most_abundant_species
