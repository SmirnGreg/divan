from divan.ReaderANDES2 import ReaderANDES2
from divan.ReaderANDES import ReaderANDES


class Reader(ReaderANDES2, ReaderANDES):
    """
    MixIn class containing reading methods for ANDES2
    """
    pass
