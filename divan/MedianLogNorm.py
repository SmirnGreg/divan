import matplotlib.colors
import numpy as np


class MedianLogNorm(matplotlib.colors.LogNorm):
    """
    Normalize a given value to the 0-1 range on a log scale, with 0.5 equals to median
    """

    def __init__(self, median=None, *args, **kwargs):
        super(MedianLogNorm, self).__init__(*args, **kwargs)
        if median is None: median = 0.5 * (self.vmin + self.vmax)  # todo?
        self.median = median

    def __call__(self, value, clip=None):
        if clip is None:
            clip = self.clip

        result, is_scalar = self.process_value(value)

        result = np.ma.masked_less_equal(result, 0, copy=False)

        self.autoscale_None(result)
        vmin, vmax, median = self.vmin, self.vmax, self.median

        if not (vmin <= median <= vmax):
            raise ValueError("minvalue must be less than or equal to maxvalue")
        if vmin > vmax:
            raise ValueError("minvalue must be less than or equal to maxvalue")
        elif vmin <= 0:
            raise ValueError("values must all be positive")
        elif vmin == vmax:
            result.fill(0)
        else:
            if clip:
                mask = np.ma.getmask(result)
                result = np.ma.array(np.clip(result.filled(vmax), vmin, vmax),
                                     mask=mask)
            # in-place equivalent of above can be much faster
            resdat = result.data
            mask = result.mask
            if mask is np.ma.nomask:
                mask = (resdat <= 0)
            else:
                mask |= resdat <= 0
            np.copyto(resdat, 1, where=mask)

            index_less = np.where(resdat < median)
            index_higher = np.where(resdat >= median)
            less_median_values = resdat[index_less]
            higher_median_values = resdat[index_higher]
            less_median_values = 0.5 * (np.log(less_median_values) - np.log(vmin)) / (np.log(median) - np.log(vmin))
            higher_median_values = 0.5 * (np.log(higher_median_values) - np.log(median)) / (
                    np.log(vmax) - np.log(median)) + 0.5
            resdat[index_less] = less_median_values
            resdat[index_higher] = higher_median_values

            # np.log(resdat, resdat)
            # resdat -= np.log(vmin)
            # resdat /= (np.log(vmax) - np.log(vmin))
            result = np.ma.array(resdat, mask=mask, copy=False)
        if is_scalar:
            result = result[0]
        return result


