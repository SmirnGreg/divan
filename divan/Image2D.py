import math
import warnings
import logging

import numpy as np
from scipy import interpolate
import matplotlib.cm
from matplotlib import colors, pyplot as plt

from divan.plot_functions import generate_2d_disk_figure
from divan.Chemistry import Chemistry
from divan.Physics import Physics

module_logger = logging.getLogger("divan.2d")


class Image2D(Chemistry, Physics):
    """
    MixIn class for 2D figures (chemistry, physics, etc...)
    """

    def add_extra_contours(  # TODO: make Divan2DBase ?
            self,
            axes=None,
            r=None, z=None,
            contour_data=None,
            levels=None,
            contour_data_columnname=None,
            contour_kwargs=None,
            both_panels=True,
            clabel=None,
            do_interpolate=None,
            interpolate_logarithms=False,
            interpolation_kwargs=None,
    ):
        """
        Method to add contours of some data on axes

        :param axes: matplotlib axes to be overplotted
        :param r: radial data array
        :param z: height data array
        :param contour_data: contour data array
        :param levels: levels to plot
        :param contour_data_columnname: column
        :param contour_kwargs:
        :param both_panels:
        :param clabel:
        :param do_interpolate:
        :param interpolate_logarithms:
        :param interpolation_kwargs:
        """
        if axes is None:
            axes = self.figures[-1].axes[0]
        contour_kwargs = contour_kwargs or {}
        interpolation_kwargs = interpolation_kwargs or {}
        levels_sorted = sorted(levels)
        module_logger.info(f"Generating extra contours of {contour_data_columnname}")
        if (r is None) or (z is None) or (contour_data is None):
            try:
                r, z = (
                    self.physical_structure['Radius'],
                    self.physical_structure['Height'] / self.physical_structure['Radius'],
                )
                if contour_data_columnname in self.physical_structure.keys():
                    contour_data = self.physical_structure[contour_data_columnname]
                elif contour_data_columnname in self.chemical_structure.keys():
                    contour_data = self.chemical_structure[contour_data_columnname]
                else:
                    raise KeyError(
                        "No '{}' found in read physical and chemical structure!".format(contour_data_columnname)
                    )
            except:  # TODO make better exception
                raise AttributeError(
                    'No data given for add_extra_contours(). Use read_physical_structure() or send arguments')

        if do_interpolate is None:
            contour_top = axes.tricontour(r, z, contour_data, levels=levels_sorted, **contour_kwargs)
            if both_panels:
                contour_bottom = axes.tricontour(r, -z, contour_data, levels=levels_sorted, **contour_kwargs)
        else:
            r_plot = np.logspace(math.log10(min(r)), math.log10(max(r)), do_interpolate)
            z_plot = np.linspace(min(z), max(z), do_interpolate)
            R_plot, Z_plot = np.meshgrid(r_plot, z_plot)

            contour_data_to_interpolate = np.log10(
                contour_data) if interpolate_logarithms else contour_data
            contour_data_interpolated = interpolate.griddata(
                (r, z), contour_data_to_interpolate,
                (R_plot, Z_plot),
                **interpolation_kwargs)
            if interpolate_logarithms:
                contour_data_interpolated = np.power(10, contour_data_interpolated)

            contour_top = axes.contour(R_plot, Z_plot, contour_data_interpolated,
                                       levels=levels_sorted, **contour_kwargs)
            if both_panels:
                contour_bottom = axes.contour(R_plot, -Z_plot, contour_data_interpolated,
                                              levels=levels_sorted, **contour_kwargs)

        if clabel is not None:
            axes.clabel(contour_top, fmt=clabel, use_clabeltext=True)
            # if both_panels:
            #     axes.clabel(contour_bottom, fmt=clabel, use_clabeltext=True)

    def generate_figure(
            self,
            r=None, z=None,
            data1=None, data2=None,
            do_interpolate=None,  # Points to interpolate, no interpolation if False
            interpolation_kwargs=None,
            title=None,
            contours_kwargs=None,
            image_kwargs=None,
            levels=None,
            normalizer=colors.LogNorm(),
            axes=None,
            colorbar_label=None,
            **kwargs  # To be passed to generate_2d_disk_figure
    ):
        """
        This method returns figure with generated image, replaces self.figure_chemistry with it and appends it to self.figures list.

        :param r:
        :param z:
        :param data1:
        :param data2:
        :param do_interpolate:
        :param interpolation_kwargs:
        :param contours_kwargs:
        :param image_kwargs:
        :param levels:
        :param normalizer:
        :param axes:
        :param kwargs:
        :return:
        """
        interpolation_kwargs = interpolation_kwargs or {}
        contours_kwargs = contours_kwargs or {}
        image_kwargs = image_kwargs or {}
        if isinstance(data1, str):
            module_logger.info("Generating figure of: ", data1, data2)
        if 'cmap' not in image_kwargs.keys():
            image_kwargs['cmap'] = matplotlib.cm.get_cmap('viridis', 10)
        levels_sorted = sorted(levels) if levels else None
        if (r is None) or (z is None):
            try:
                r, z = (
                    self.physical_structure['Radius'],
                    self.physical_structure['Relative Height'],
                )
                module_logger.info("Parameters successfully taken from self")
            except:  # TODO make better exception
                raise AttributeError(
                    'No coordinates data given for .generate_figure(). Use .read_physical_structure() or send arguments')
        if type(data1) in (str, np.str, np.str_):
            try:
                data1 = (self.chemical_structure[data1])
            except KeyError:
                data1 = None
            except:
                raise AttributeError(
                    f'No {data1} data given for .generate_figure(). Use .read_structure() or send arguments')
        if type(data2) in (str, np.str, np.str_):
            try:
                data2 = (self.physical_structure[data2])
            except KeyError:
                warnings.warn(f"No data found for {data2}, {data1} used instead!", UserWarning)
                data2 = data1
                # spec2 = None
            except:
                raise AttributeError(
                    f'No {data2} data given for .generate_figure(). Use .read_physical_structure() or send arguments')
        if axes is None:
            self.last_figure, axes = plt.subplots()
            self.figures.append(self.last_figure)
            module_logger.debug('Generate axes')
        if self.titles: axes.set_title(title)
        axes.set_xlabel('Radius [au]')
        axes.set_ylabel('Height/Radius')

        fig = generate_2d_disk_figure(
            r, z, top_panel_data=data1, top_panel_levels=levels_sorted,
            bottom_panel_data=data2,
            top_image_kwargs=image_kwargs,
            top_interpolate=do_interpolate, axes=axes, colorbar_label=colorbar_label,
            top_interpolation_kwargs=interpolation_kwargs, top_contours_kwargs=contours_kwargs,
            top_interpolate_logarithms=True, normalizer=normalizer, **kwargs
        )
        return fig
