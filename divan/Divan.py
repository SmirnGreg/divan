import os
import datetime
import logging
import time
from warnings import warn

import astropy.io.ascii as ascii
from matplotlib.backends.backend_pdf import PdfPages
# from matplotlib.backends.backend_pgf import PdfPages # TODO figure out
from matplotlib import style

from divan.Plot1D import Plot1D
from divan.Image2D import Image2D
from divan.Reader import Reader
from divan.TextPage import TextPage
from divan.ReaderANDES import ReaderANDES

divan_logger = logging.getLogger("divan")


class Divan(Reader, Plot1D, Image2D, TextPage):
    """
    Class for plotting ANDES output

    The functionality is operated by parent MixIn classes Reader, Plot1D, Image2D, TextPage
    """

    def __init__(
            self,
            input_folder='../data_output/',
            input_format='ANDES2',
            verbosity=1,
            matplotlib_style=os.path.join(os.path.abspath(os.path.dirname(__file__)), 'divan.mplstyle'),
            titles=True,
            *args, **kwargs
    ):
        self.inittime = datetime.datetime.now()
        self.verbosity = verbosity
        self.input_folder = input_folder
        try:
            style.use(os.path.abspath(os.path.dirname(__file__)) + '/' + matplotlib_style)
        except OSError as e0:
            try:
                style.use(matplotlib_style)
            except OSError as e:
                divan_logger.error(e)
                divan_logger.error(e0)

        self.figures = list()
        self.physical_structure = None
        self.chemical_structure = None
        self.chemistry_normalize_to = None
        self.titles = titles

        # Define reader for astropy.io.ascii
        if input_format == 'ANDES2':
            self.reader = ascii.get_reader(Reader=ascii.CommentedHeader)
            self.reader.header.splitter.delimiter = '|'  # usage: reader.read('path/to/file')

            self.read_physical_structure = self._read_physical_structure_andes2
            self.read_chemical_structure = self._read_chemical_structure_andes2
            self.read_chemitry_totalmass = self._read_chemitry_totalmass_andes2
            self.read_surface_densities = self._read_surface_densities_andes2
            self.read_gtb = self._read_gtb_andes2

        elif input_format == 'ANDES1':
            self.reader = ascii.get_reader(Reader=ascii.FixedWidth)
            self.read_physical_structure = self._read_physical_structure_andes1
            self.read_chemical_structure = self._read_chemical_structure_andes1
            self.read_chemitry_totalmass = NotImplemented
            self.read_surface_densities = NotImplemented
            self.read_gtb = NotImplemented

        else:
            raise IOError(f'No {input_format} reader found!')

        self.tuple_of_species_for_most_abundant = None
        self.skip_pages_most_total_abundant = 0
        divan_logger.info("Divan object {} initialized".format(self))

    def __str__(self):
        out_string = "Divan object\ncontains:\n"
        out_string += "Current reader is " + str(self.reader) + "\n"
        if self.physical_structure:
            out_string += "Physical structure was read\n"
        if self.chemical_structure:
            out_string += "Chemical structure was read\n"
            if self.tuple_of_species_for_most_abundant:
                out_string += "Current most abundant species are selected from " + \
                              str(self.tuple_of_species_for_most_abundant) + "\n"
        out_string += "{} figures:\n".format(len(self.figures))
        for figure in self.figures:
            out_string += repr(figure) + "\n"
        return out_string

    def save_figures_pdf(
            self,
            output_filename='multipage.pdf',
            size_inches=(6, 4),
            a4=False,
            tight_layout=False,
    ):
        """Save all figures from Divan instance to multiple-paged pdf"""
        if a4: size_inches = (8.3, 11.7)
        if tight_layout:
            warn("There are bugs with matplotlib Axes.set_yticks and tight_layout...", UserWarning)
        with PdfPages(output_filename) as output_file:
            for i, figure in enumerate(self.figures):
                divan_logger.info(
                    "Drawing figure {i} of {n}: {fig}".format(i=i + 1, n=len(self.figures), fig=repr(figure)))
                figure.set_size_inches(*size_inches, forward=True)
                for attempt in range(10):
                    try:
                        if tight_layout:
                            figure.tight_layout()
                        output_file.savefig(figure)
                        break
                    except TimeoutError as e:
                        divan_logger.error(f"Attempt {attempt + 1}: {e}")
                        time.sleep(10)
        divan_logger.info(f"Saved to file {os.path.abspath(output_filename)}")
        return

    def show_interactively(self, *args, **kwargs):
        # plt.show(*args, **kwargs)
        return NotImplemented
