import numpy as np
import warnings

try:
    import radmc3dPy
except ImportError:
    warnings.warn("radmc3dpy not found! Download it from https://github.com/keflavich/radmc3dPy", ImportWarning)

class OutputRADMC3D:
    """
    Mix-in Class to output grid and physical/chemical structure in RADMC3D format
    """
    def make_r_theta_grid(
            self, *args, **kwargs
            # nr=100, ntheta=100,
            # minr=1, maxr=200,
    ):
        grid = radmc3dPy.reggrid.radmc3dGrid(

        )
