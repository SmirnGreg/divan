import logging
from copy import deepcopy
from warnings import warn
from collections import Counter

import numpy as np
from astropy.table.table import Table

from divan.interpolation import interpolate


def return_first_argument(x, *args, **kwargs):
    """This decorator does nothing"""
    return x


module_logger = logging.getLogger("divan.tableplus")

try:
    from tqdm import tqdm
except ImportError:
    warn("tqdm was not found. Install tqdm to see progress bars.", ImportWarning)
    tqdm = return_first_argument


def check_dimensions(method):
    """Decorator to check dimensions and whatever before executing method"""

    def wrapper(self, other, *args, **kwargs):
        if len(self) != len(other):
            raise TypeError("Tables have different size! Use TablePlus.interpolate method first.")
        if np.any(self.columns[0] - other.columns[0]) or np.any(self.columns[1] - other.columns[1]):
            raise TypeError("Tables have different coordinates! Use TablePlus.interpolate method first.")

        return method(self, other, *args, **kwargs)

    return wrapper


class TablePlus(Table):
    """
    divan.TablePlus is a subclass to astropy.table.Table, that allows arithmetic (+, -, *, /) operation.
    They are performed on columns[2:], so Radius and Height columns are not affected.
    """

    @check_dimensions
    def __add__(self, other):
        out = deepcopy(self)
        for column in self.columns[2:]:
            out[column] = self[column] + other[column]
        return out

    @check_dimensions
    def __sub__(self, other):
        out = deepcopy(self)
        for column in self.columns[2:]:
            out[column] = self[column] - other[column]
        return out

    @check_dimensions
    def __mul__(self, other):
        out = deepcopy(self)
        for column in self.columns[2:]:
            out[column] = self[column] * other[column]
        return out

    @check_dimensions
    def __truediv__(self, other):
        out = deepcopy(self)
        for column in self.columns[2:]:
            out[column] = self[column] / other[column]
        return out

    def __getitem__(self, item):
        try:
            return super().__getitem__(item)
        except KeyError:
            pass

        if " / " in item:
            key1, key2 = item.split(" / ")
            out = self[key1] / self[key2]
            out.name = item
            return out

        else:
            try:
                return super().__getitem__(item)
            except KeyError:
                module_logger.debug("Have not found %s in table, resolving...", item)
                dictionary_with_keys = self.resolve_key_formula(item)
                module_logger.debug("Resolved as %s", dictionary_with_keys)
            out = 0.
            for key, value in dictionary_with_keys.items():
                out += value * super().__getitem__(key)
            out.name = item
            return out

    @staticmethod
    def resolve_key_formula(formula):
        elements = formula.split(" + ")
        elements_out = []
        for element in elements:
            if " * " in element:
                mult, spec = element.split(" * ")
                elements_out.extend(int(mult) * [spec])
            else:
                elements_out.append(element)
        return Counter(elements_out)

    def interpolate(self, other, progressbar=False, **kwargs):
        """
        Interpolate table to coordinates of other table
        :param other: table to take coordinates from (first two column)
        :param progressbar: use tqdm if True
        :return: table with new coordinates
        """
        if len(self) == len(other):
            if not (np.any(self.columns[0] - other.columns[0]) or np.any(self.columns[1] - other.columns[1])):
                # no need for interpolation
                module_logger.info("no interpolation")
                return self
        if progressbar:
            tqdm_dec = tqdm
        else:
            tqdm_dec = return_first_argument
        out = deepcopy(other)

        x0 = deepcopy(self.columns[0])
        y0 = deepcopy(self.columns[1])

        x1 = other.columns[0]
        y1 = other.columns[1]

        out[x1.name] = x1
        out[y1.name] = y1

        for column in tqdm_dec(self.columns[2:]):
            out[column] = interpolate(x0, y0, self[column], x1, y1, **kwargs)
            # scipy.interpolate.griddata(np.vstack((x0, y0)).T, self[column], np.vstack((x1, y1)).T)
        return out
