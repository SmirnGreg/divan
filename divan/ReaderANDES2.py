import copy
import glob
import logging
from warnings import warn

import numpy as np

module_logger = logging.getLogger("divan.reader")


class ReaderANDES2:
    """
    MixIn class containing reading methods for ANDES2
    """

    def _read_surface_densities_andes2(
            self, data_file=None
    ):
        """
        Method to read and store surface densities from file

        :param data_file: path to file with input table, default '../data_output/surface_density'
        :return: astropy.table.Table with surface density structure
        """
        data_file = data_file or self.input_folder + 'surface_density'
        self.surface_densities_data = self.reader.read(data_file)
        if np.any(np.abs(self.surface_densities_data['integ(gas_density)/sigma_gas'] - 1.) > 1e-4):
            warn(
                'Something probably went wrong: integrated volume gas density is not consisted with surface gas density'
            )
        return self.surface_densities_data

    def _read_physical_structure_andes2(
            self,
            data_file=None,
            data_file_template=None,
    ):
        """
        Method to read and store physical structure from file

        :param data_file: path to file with input table
        :param data_file_template: file template to select the latest, default r'../data_output/physical_structure_*'
        :param verbosity:
        :return: astropy.table.Table with physical structure
        """
        data_file_template = data_file_template or self.input_folder + r'physical_structure*'
        if data_file is None:
            file_list = glob.glob(data_file_template)
            file_list.sort()
            data_file = file_list[-1]  # Take alphabetically last file in folder
            module_logger.info('Reading {file}...'.format(file=data_file))
        self.physical_structure = self.reader.read(data_file)
        try:
            self.physical_structure.remove_columns(['ir', 'iz'])
        except KeyError:
            pass
        # self.physical_structure.remove_column('iz')
        return self.physical_structure

    def _read_chemical_structure_andes2(
            self,
            data_file=None,
            data_file_template=None,
    ):
        # TODO pickling: 8 sec @ 100x100 @ mac-smirnov with astropy.io.ascii vs 0.15 sec with pickle
        """
                Method to read and store chemical structure from file

                :param data_file: path to file with input table
                :param data_file_template: file template to select the latest, default r'../data_output/Chemistry_0*'
                :param verbosity:
                :return: astropy.table.Table with chemical structure
                """
        data_file_template = data_file_template or self.input_folder + r'Chemistry_0*'
        if data_file is None:
            file_list = glob.glob(data_file_template)
            file_list.sort()
            data_file = file_list[-1]  # Take alphabetically last file in folder
        module_logger.info('Reading {file}...'.format(file=data_file))
        self.chemical_structure = self.reader.read(data_file)
        self.chemical_structure_initial = copy.deepcopy(self.chemical_structure)
        self.chemistry_normalize_to = None
        return self.chemical_structure

    def _read_chemitry_totalmass_andes2(
            self,
            data_file=None,
            data_file_template=None,
    ):
        """
        Method to read and store total mass from file

        :param data_file: path to file with input table
        :param data_file_template: file template to select the latest, default r'../data_output/Chemistry_totmass_0*'
        :return: astropy.table.Table with chemical structure
        """
        data_file_template = data_file_template or self.input_folder + r'Chemistry_totmass_0*'
        if data_file is None:
            file_list = glob.glob(data_file_template)
            file_list.sort()
            data_file = file_list[-1]  # Take alphabetically last file in folder
        module_logger.info('Reading {file}...'.format(file=data_file))
        self.total_abundances = self.reader.read(data_file)
        return self.total_abundances

    def _read_gtb_andes2(
            self,
            data_file=None,
            data_file_template=None,
    ):
        """
        Method to read and store gas thermal balance data from file

        :param data_file: path to file with input table
        :param data_file_template: file template to select the latest, default r'../data_output/GTB_*'
        :return: astropy.table.Table with chemical structure
        """
        data_file_template = data_file_template or self.input_folder + r'GTB_*'
        if data_file is None:
            file_list = glob.glob(data_file_template)
            file_list.sort()
            data_file = file_list[-1]  # Take alphabetically last file in folder
        module_logger.info('Reading {file}...'.format(file=data_file))
        self.gtb = self.reader.read(data_file)
        return self.gtb
