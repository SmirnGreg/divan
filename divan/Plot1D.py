from warnings import warn
import logging

import numpy as np
import astropy.table
from cycler import cycler
from matplotlib import pyplot as plt, cm, patches
from scipy.interpolate import interp1d
from scipy.ndimage.filters import gaussian_filter1d, gaussian_filter

from divan.SigRangeNorm import SigRangeNorm
from divan._DivanBase import DivanBase
from divan.names import get_name

module_logger = logging.getLogger("divan.1d")


class Plot1D(DivanBase):
    """
    MixIn class for 1D plotting
    """

    def generate_figure_surface_densities(
            self,
            r=None,
            dust_density=None, gas_density=None,
            gas_style=None,  # 'b-',
            dust_style=None,  # 'r--',
    ):
        module_logger.info("Generating figure of surface denstiies")
        if (r is None) or (dust_density is None) or (gas_density is None):
            try:
                r, dust_density, gas_density = (
                    self.surface_densities_data['Radius'],
                    self.surface_densities_data['Dust surface density'],
                    self.surface_densities_data['Gas surface density'],
                )
                module_logger.debug("Parameters successfully taken from self")
            except:
                raise AttributeError(
                    'No data given for generate_figure_surface_densities(). Use read_surface_densities() or send arguments')
        self.figure_surface_densities, ax_surface_densities = plt.subplots()

        if self.titles: ax_surface_densities.set_title('Gas and dust surface density')
        ax_surface_densities.set_xlabel('Radius [au]')
        ax_surface_densities.set_ylabel(r'Surface density $\Sigma$, g cm$^{-2}$')
        ax_surface_densities.loglog()
        ax_surface_densities.autoscale(enable=True, axis='x', tight=True)

        if dust_style is not None:
            ax_surface_densities.plot(r, dust_density, dust_style, label='Dust')
        else:
            ax_surface_densities.plot(r, dust_density, label='Dust')
        if gas_style is not None:
            ax_surface_densities.plot(r, gas_density, gas_style, label='Gas')
        else:
            ax_surface_densities.plot(r, gas_density, label='Gas')
        ax_surface_densities.legend()
        self.figures.append(self.figure_surface_densities)
        return self.figure_surface_densities

    def generate_plot(
            self,
            r=None,
            data=None,
            data_up=None,
            data_low=None,
            title=None,
            xlabel='Radius [au]',
            ylabel=None,
            loglog=None,
            labels=(),
            xlim=None,
            ylim=None,
            log=None,
            gap=None,
            gap_kwargs=None,
            grid=False,
            convolve=None,
            xlog=True,
            ylog=True,
            and_their_ratio=False,
            colors=(),
            fill_between_kwargs=None,
            line_cycle=None,
            max_legend_items=np.inf,
            extra_legend_items=None,
            legend_kwargs=None,
            color_legend_labels=False,
            **kwargs,
    ):
        module_logger.debug(f"Generating 1D plot")
        if gap_kwargs is None:
            gap_kwargs = {
                "color": "black",
                "alpha": 0.1,
                "hatch": '//\\\\',
            }
        if legend_kwargs is None:
            legend_kwargs = {}
        if log is not None:
            module_logger.warning("Keyword log is deprecated, use xlog/ylog instead!")
            warn("Keyword log is deprecated, use xlog/ylog instead!", DeprecationWarning)
        if loglog is not None:
            module_logger.warning("Keyword loglog is deprecated, use xlog/ylog instead!")
            warn("Keyword loglog is deprecated, use xlog/ylog instead!", DeprecationWarning)
        if and_their_ratio and len(data) != 2:
            module_logger.warning(
                "Keyword and_their_ratio is not implemented if the number of elements in data is not equal 2"
            )

        if line_cycle is None:
            if len(data) <= 10:
                line_cycle = cycler(color=cm.get_cmap('tab10')(np.arange(10)))
            elif len(data) <= 20:
                line_cycle = cycler(color=cm.get_cmap('tab20')(np.arange(20)))
            else:
                line_cycle = (
                        cycler(color=cm.get_cmap('tab10')(np.arange(10)))
                        * cycler(marker=['+', ',', '.'])
                        * cycler(linestyle=['-', '--', '-.'])
                )
        if (r is None):
            try:
                r = self.surface_densities_data['Radius']
                module_logger.debug("Parameters successfully taken from self")
            except:  # TODO make better exception
                raise AttributeError(
                    f'No data given for {__name__}. Use read_surface_densities() or send arguments')

        self.last_figure, ax = plt.subplots()
        if self.titles: ax.set_title(title)
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        ax.set_prop_cycle(line_cycle)

        if xlog:
            ax.set_xscale("log")
        # else:
        #     ax.set_xscale("linear")
        if ylog:
            ax.set_yscale("log")
        # else:
        #     ax.set_yscale("linear")
        for_legend_entities = []
        for_legend_labels = []
        legend_items = 0
        for i, entry in enumerate(data):
            if isinstance(entry, str):
                try:
                    y = self.surface_densities_data[entry]
                except KeyError:
                    warn(
                        f'No {entry} given for {__name__}. Use read_surface_densities() or send arguments',
                        UserWarning
                    )
                    continue
            elif len(entry) == len(r):
                y = entry
            else:
                warn(
                    f'Wrong data with index {i} was given for {__name__}.',
                    UserWarning
                )
                continue
            try:
                name = get_name(labels[i])
            except (KeyError, IndexError):
                name = None
            if name is None:
                try:
                    name = get_name(y.name)
                except AttributeError:
                    module_logger.warning("Name not found!")
                    name = str(i)
            try:
                data_low_to_plot = data_low[i]
                data_up_to_plot = data_up[i]
            except (IndexError, TypeError):
                module_logger.debug(f"Data low or data up not found for {i}")
                data_low_to_plot = None
                data_up_to_plot = None

            if convolve is not None:
                module_logger.debug("Convolving 1d plot")
                r_interp = np.arange(1001)
                y_interp = interp1d(r, y, bounds_error=False, fill_value='extrapolate')(r_interp)
                y_smooth = astropy.table.Column(gaussian_filter1d(y_interp, convolve), name=name)
                if data_up_to_plot is not None and data_low_to_plot is not None:
                    data_low_to_plot = gaussian_filter1d(
                        interp1d(
                            r, data_low_to_plot, bounds_error=False, fill_value='extrapolate'
                        )(r_interp),
                        convolve
                    )
                    data_up_to_plot = gaussian_filter1d(
                        interp1d(
                            r, data_up_to_plot, bounds_error=False, fill_value='extrapolate'
                        )(r_interp),
                        convolve
                    )

                r_to_plot = r_interp
                y_to_plot = y_smooth
            else:
                r_to_plot = r
                y_to_plot = y

            if i == 0:
                name0 = name
                y0 = y_to_plot
            if i == 1:
                name1 = name
                y1 = y_to_plot
            color = None
            if colors:
                try:
                    if isinstance(colors, str):
                        raise IndexError()
                    color = colors[i]
                except IndexError:
                    color = colors
                except (ValueError, TypeError):
                    module_logger.warning(
                        "Something wrong with colors %s! It should be data-sized list of strings, or a single string.",
                        colors)

            if data_low_to_plot is not None and data_up_to_plot is not None:
                line = ax.plot(r_to_plot, y_to_plot, color=color, **kwargs)
                fb_kwargs = {}
                try:
                    fb_kwargs = fill_between_kwargs[i]
                except IndexError:
                    fb_kwargs = fill_between_kwargs
                except:
                    fb_kwargs = {}
                fill = ax.fill_between(
                    r_to_plot,
                    data_low_to_plot,
                    data_up_to_plot,
                    alpha=0.4,
                    facecolor=line[0].get_color(),
                    label=name,
                    **fb_kwargs
                )
                if legend_items < max_legend_items:
                    legend_items += 1
                    for_legend_entities.append(fill)
                    for_legend_labels.append(name)
            else:
                line = ax.plot(r_to_plot, y_to_plot, label=name, color=color, **kwargs)
                if legend_items < max_legend_items:
                    legend_items += 1
                    for_legend_entities.append(line[0])
                    for_legend_labels.append(name)

        ax.set_xlim(xlim)
        ax.set_ylim(ylim)
        if grid:
            ax.grid(which='both')
        if gap:
            ax.axvspan(*gap, **gap_kwargs)
        if and_their_ratio and len(data) == 2:
            ax_twin = ax.twinx()
            ax_twin.semilogy()
            # ax.tick_params(axis='y', labelcolor=color, which='both')
            color = 'C3'
            ratio_line = ax_twin.plot(
                r_to_plot,
                y0 / y1,
                label=f"{name0}/{name1}",
                color=color
            )
            for_legend_entities.append(ratio_line[0])
            for_legend_labels.append(f"{name0} / {name1} ratio")
            ax.axhline(1, color=color, alpha=0.1)
        if extra_legend_items is not None:
            try:
                for_legend_entities.extend(extra_legend_items['entities'])
                for_legend_labels.extend(extra_legend_items['labels'])
            except (KeyError, TypeError):
                module_logger.error("Invalid extra_legend_items: %s", extra_legend_items)

        leg = ax.legend(for_legend_entities, for_legend_labels, **legend_kwargs)
        if color_legend_labels or not legend_kwargs.get('handlelength', True):
            if leg.get_lines():
                for line, text in zip(leg.get_lines(), leg.get_texts()):
                    text.set_color(line.get_color())
            else:
                for line, text in zip(leg.get_patches(), leg.get_texts()):
                    text.set_color(line.get_facecolor()[0:3])
        self.figures.append(self.last_figure)
        return self.last_figure

    def generate_disk_from_radial_profile(
            self,
            r=None,
            data=None,
            title=None,
            max_r=700,
            min_r=1,
            npoints=200,
            xlabel='X, [au]',
            ylabel='Y, [au]',
            norm=SigRangeNorm(),
            convolve=None,
            **kwargs,
    ):
        module_logger.info(f"Generating disk image")

        if (r is None):
            try:
                r = self.surface_densities_data['Radius']
                module_logger.debug("Parameters successfully taken from self")
            except:  # TODO make better exception
                raise AttributeError(
                    f'No data given for {__name__}. Use read_surface_densities() or send arguments')

        self.last_figure, ax = plt.subplots(subplot_kw={'aspect': 'equal'})
        if self.titles: ax.set_title(title)
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        if isinstance(data, str):
            try:
                y = self.surface_densities_data[data]
            except KeyError:
                raise AttributeError(
                    f'No {data} given for {__name__}. Use read_surface_densities() or send arguments',
                )
        elif len(data) == len(r):
            y = data
        else:
            raise AttributeError(
                f'Wrong data was given for {__name__}.',
            )

        if convolve is not None:
            fill_value = 'extrapolate'
        else:
            fill_value = None
        interpolated = interp1d(r, y, kind='nearest', bounds_error=False, fill_value=fill_value)

        x_coord = np.linspace(-max_r, max_r, npoints)
        y_coord = np.linspace(-max_r, max_r, npoints)

        X, Y = np.meshgrid(x_coord, y_coord)
        Z = np.zeros_like(X)

        Z = interpolated(np.sqrt(X ** 2 + Y ** 2))
        Z[np.where(np.sqrt(X ** 2 + Y ** 2) < min_r)] = np.nan
        # with np.nditer(Z, flags=['multi_index']) as it:
        #     while not it.finished:
        #         Z[it.multi_index] = interpolated(X[it.multi_index] ** 2 + Y[it.multi_index] ** 2)
        #         it.iternext()
        if convolve is not None:
            Z = gaussian_filter(Z, convolve)
            ellipse = patches.Ellipse(
                (-0.8 * max_r, -0.8 * max_r),
                convolve * 2.355, convolve * 2.355,
                color='white',
                alpha=0.5
            )
            ax.add_artist(ellipse)
        image = ax.pcolormesh(X, Y, Z, shading='gourand', rasterized=True, norm=norm, **kwargs)
        self.last_figure.colorbar(image, ax=ax)
        self.figures.append(self.last_figure)
        return self.last_figure
