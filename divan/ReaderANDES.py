import glob
import copy
import logging

import astropy.io.ascii
from astropy.table.table import Table
import astropy.io.ascii

module_logger = logging.getLogger("divan.reader")


def translator_physics(data_file, *args, **kwargs):
    reader = astropy.io.ascii.get_reader(astropy.io.ascii.CommentedHeader)
    in_table = reader.read(data_file, *args, **kwargs)
    names_dict = {
        'Radius': 'R[a.u.]',
        'Height': 'z[a.u.]',
        'Relative Height': 'z/R',
        'Dust temperature': 'Tdust[K]',
        'Gas temperature': 'Tgas[K]',
        'Dust density': 'rho_dust[g/cm3]',
        'Gas density': 'rho_gas[g/cm3]',
        'Dust to gas': 'd2g',
        'AV_true': 'AvIS'
    }
    columns = []
    for name in names_dict:
        column = in_table[names_dict[name]]
        column.name = name
        columns.append(column)
    return Table(columns)


class ReaderANDES:

    def _read_physical_structure_andes1(
            self,
            data_file=None,
            data_file_template=None,
    ):
        """
        Method to read and store physical structure from file

        :param data_file: path to file with input table
        :param data_file_template: file template to select the latest, default r'../data_output/physical_structure_*'
        :param verbosity:
        :return: astropy.table.Table with physical structure
        """
        data_file_template = data_file_template or self.input_folder + r'physical_structure*'
        if data_file is None:
            file_list = glob.glob(data_file_template)
            file_list.sort()
            data_file = file_list[-1]  # Take alphabetically last file in folder
        module_logger.info('Reading {file}...'.format(file=data_file))
        self.physical_structure = translator_physics(data_file)
        try:
            self.physical_structure.remove_columns(['ir', 'iz'])
        except KeyError:
            pass
        # self.physical_structure.remove_column('iz')
        return self.physical_structure

    def _read_chemical_structure_andes1(
            self,
            data_file=None,
    ):
        """
                Method to read and store chemical structure from file

                :param data_file: path to file with input table
                :param verbosity:
                :return: astropy.table.Table with chemical structure
                """
        module_logger.info('Reading {file}...'.format(file=data_file))
        self.chemical_structure = self.reader.read(data_file)
        colnames = self.chemical_structure.colnames
        colnames.remove('Radius')
        colnames.remove('Height')
        new_colnames = ['Radius', 'Height']
        new_colnames.extend(colnames)
        self.chemical_structure = self.chemical_structure[new_colnames]
        self.chemical_structure_initial = copy.deepcopy(self.chemical_structure)
        self.chemistry_normalize_to = None
        return self.chemical_structure
