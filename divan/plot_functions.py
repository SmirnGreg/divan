"""
Module containing generate_2d_disk_figure, the core function of divan package, that plots all 2D images in package.
"""

import math
import copy
import logging

from typing import Tuple

import numpy as np
from scipy import interpolate
from astropy.visualization import quantity_support
quantity_support()
from matplotlib import pyplot as plt
from matplotlib import colors
import matplotlib.colors
from matplotlib.ticker import FuncFormatter
import matplotlib.cm, matplotlib.ticker, matplotlib.patheffects

from divan.names import get_name

module_logger = logging.getLogger("divan.2d.plot_functions")

def generate_2d_disk_figure(
        radius,  # astropy column with x-coordinate
        height,  # astropy column with y-coordinate
        top_panel_data,  # Data to be plotted on top panel

        top_panel_levels_data=None,  # Data to be used for levels on top panel, top_panel_data by default
        top_panel_levels=None,  # Contour levels on top panel
        top_interpolate=None,  # Interpolate grid size, no interpolation if None
        top_interpolate_logarithms=False,  # Interpolate logarithm of data

        bottom_panel_data=None,  # Data to be plotted on bottom panel, no plotting if None
        bottom_panel_levels_data=None,  # Data to be used for levels on bottom panel, bottom_panel_data by default
        bottom_panel_levels=None,  # Contour levels on bottom panel, top_panel_levels by default
        bottom_interpolate=None,  # Interpolate grid size, top_interpolate by default
        bottom_interpolate_logarithms=None,  # Interpolate logarithm of data, top_interpolate_logarithms by default

        height_is_relative=False,  # Divide height by radius
        bottom_data_multiplier=1,  # Multiply data on bottom before plotting, report it
        smooth_triangulation=False,  # The same as top_image_kwargs['shading'] = 'gourand'

        top_interpolation_kwargs=None,  # kwargs to be passed to interpolation function (top panel)
        bottom_interpolation_kwargs=None,  # kwargs to be passed to interpolation function (bottom panel)
        top_contours_kwargs=None,  # kwargs to be passed to contour function (top panel)
        bottom_contours_kwargs=None,  # kwargs to be passed to contour function (top panel)
        top_image_kwargs=None,  # kwargs to be passed to image function (top panel)
        bottom_image_kwargs=None,  # kwargs to be passed to image function (top panel)
        text_kwargs=None,  # kwargs to be passed to label above image

        axes=None,  # matplotlib.axes.Axes object, matplotlib.pyplot by default

        do_colorbar=True,  # Do colorbar if True
        colorbar_label=None,  # Label on colorbar, top_panel_data.name by default
        colorbar_ticks=None,  # matplotlib.ticker.LogLocator(numticks=15),  # Ticks positions to appear on colorbar
        colorbar_ticklabels=None,  # Ticks labels to appear on colorbar
        colorbar_ticklabels_rotate=0,  # Angle to rotate colorbar ticklabels
        cbar_kwargs=None,  # kwargs to be passed to colorbar
        normalizer=None,  # a matplotlib.colors.Normalize() instance for imaging
        under_color=None,  # set color for values under min
        zerolinecolor='black',

        xlim=None,  # 2-element array, min and max on x-axis
        ylim=None,  # 2-element array, min and max on y-axis

        no_negatives_in_labels=True,  # Change ylabels to abs(ylabels), may cause errors if True

        highlight: Tuple[float, float] = (),  # 2-component tuple with locations to highlight
        highlight_kwargs=None,  # kwargs to be passed to axvspan
):
    """
    Function to plot 2D disk structure on given matplotlib axes (axes=), or creates a new figure with 1 axes (by default).

    It is the core function of the package divan.

    Takes astropy columns as data arguments.

    The axes can be organized in 2 different ways. The default one is the classical one, with one data column. This function also allows plotting 2 data columns, like dust and gas temperatures, or H2O and gH2O abundances. The second data column is plotted under z=0 axis. This behaviour turns on if bottom_panel_data is specified. To control this behaviour, many parameters have top_ and bottom_ prefixes. If bottom_ parameter is not specified, the corresponding top_ parameter is usually taken by default.

    Additionally, the function allows plotting contours. The contours can share the data with underlying image (default), or have its own data set.

    Please report all unexpected behaviour to bugtracker https://gitlab.com/ANDES-dev/ANDES2/issues or personally to G.V. Smirnov-Pinchukov.

    :param radius:  astropy column with x-coordinate
    :param height:  astropy column with y-coordinate
    :param top_panel_data: astropy column with data to be plotted on top panel
    :param top_panel_levels_data: astropy column with data to be used for levels on top panel, top_panel_data by default
    :param top_panel_levels: contour levels on top panel
    :param top_interpolate: interpolate grid size, no interpolation if None
    :param top_interpolate_logarithms: interpolate logarithms, not original value
    :param bottom_panel_data: astropy column with data to be used for levels on bottom panel, turns 2 panels mode on
    :param bottom_panel_levels_data: astropy column with data to be used for levels on bottom panel, bottom_panel_data by default
    :param bottom_panel_levels: contour levels on bottom panel
    :param bottom_interpolate: interpolate grid size, no interpolation if None
    :param bottom_interpolate_logarithms: interpolate logarithms, not original value
    :param height_is_relative: if True, divide height by radius
    :param bottom_data_multiplier: multiply bottom data by this value for comparison with top data
    :param smooth_triangulation: turn smooth_triangulation on or off
    :param top_interpolation_kwargs: keyword arguments to be passed to interpolation function
    :param bottom_interpolation_kwargs: keyword arguments to be passed to interpolation function
    :param top_contours_kwargs: keyword arguments to be passed to tricontour method
    :param bottom_contours_kwargs: keyword arguments to be passed to tricontour method
    :param top_image_kwargs: keyword arguments to be passed to imaging method
    :param bottom_image_kwargs: keyword arguments to be passed to imaging method
    :param axes: axes to plot on
    :param do_colorbar: make colorbar if True
    :param colorbar_label: label on colorbar
    :param colorbar_ticks: ticks on colorbar
    :param colorbar_ticklabels: ticklabels on colorbar
    :param cbar_kwargs: keyword arguments to be passed to colorbar method
    :param normalizer: matplotlib.colors.Normalizer instance
    :param under_color: color to use for data < normalizer.vmin
    :return: figure that contains axes with plotted image
    """
    top_interpolation_kwargs = copy.deepcopy(top_interpolation_kwargs) or {}
    top_contours_kwargs = copy.deepcopy(top_contours_kwargs) or {}
    top_image_kwargs = copy.deepcopy(top_image_kwargs) or {}
    cbar_kwargs = copy.deepcopy(cbar_kwargs) or {}
    text_kwargs = copy.deepcopy(text_kwargs) or {}
    highlight_kwargs = copy.deepcopy(highlight_kwargs) or {}

    shading = 'gouraud' if smooth_triangulation else 'flat'

    if not ('cmap' in top_contours_kwargs.keys() or 'colors' in top_contours_kwargs.keys()):
        top_contours_kwargs['cmap'] = 'cubehelix_r'
    if 'cmap' not in top_image_kwargs.keys():
        top_contours_kwargs['cmap'] = 'viridis'
    if 'cmap' in top_image_kwargs and type(top_image_kwargs['cmap']) == str:
        top_image_kwargs['cmap'] = copy.copy(matplotlib.cm.get_cmap(top_image_kwargs['cmap']))
        if under_color is not None:
            top_image_kwargs['cmap'].set_under(under_color)
    if 'method' not in top_interpolation_kwargs.keys():
        top_interpolation_kwargs['method'] = 'nearest'
    if 'bbox' not in text_kwargs.keys():
        text_kwargs['bbox'] = dict(
            boxstyle="round",
            ec=(1., 1., 1., 0.5),
            fc=(1., 1., 1., 0.7),
        )
    if 'color' not in highlight_kwargs.keys():
        highlight_kwargs['color'] = 'white'
    if 'alpha' not in highlight_kwargs.keys():
        highlight_kwargs['alpha'] = 0.2

    if top_panel_levels_data is None:
        top_panel_levels_data = top_panel_data
    if bottom_panel_data is not None:
        if bottom_panel_levels_data is None:
            bottom_panel_levels_data = bottom_panel_data
        if bottom_panel_levels is None:  # TODO think about it
            bottom_panel_levels = top_panel_levels
        if bottom_interpolate is None:
            bottom_interpolate = top_interpolate
        if bottom_interpolation_kwargs is None:
            bottom_interpolation_kwargs = top_interpolation_kwargs
        if bottom_contours_kwargs is None:
            bottom_contours_kwargs = top_contours_kwargs
        if bottom_image_kwargs is None:
            bottom_image_kwargs = top_image_kwargs
        if bottom_interpolate_logarithms is None:
            bottom_interpolate_logarithms = top_interpolate_logarithms
    if colorbar_label is None:
        colorbar_label = get_name(top_panel_data.name)
    if height_is_relative:
        z = height
    else:
        z = height / radius

    if axes is None:
        figure, axes = plt.subplots()

    axes.semilogx()
    axes.autoscale(enable=True, axis='x', tight=True)
    axes.autoscale(enable=True, axis='y', tight=True)
    normalizer = normalizer or colors.LogNorm()#vmin=min(top_panel_data), vmax=max(top_panel_data))

    if top_interpolate is None:
        drawing_top = axes.tripcolor(
            radius, z, top_panel_data, norm=normalizer,
            shading=shading,
            # rasterized=True,
            **top_image_kwargs
        )
        if top_panel_levels is not None:
            contours_top = axes.tricontour(
                radius, z, top_panel_levels_data, norm=normalizer, levels=top_panel_levels,
                **top_contours_kwargs
            )
    else:
        r_plot = np.logspace(math.log10(min(radius)), math.log10(max(radius)), top_interpolate)
        z_plot = np.linspace(min(z), max(z), top_interpolate)

        R_plot, Z_plot = np.meshgrid(r_plot, z_plot)

        top_panel_data_to_interpolate = np.log10(top_panel_data) if top_interpolate_logarithms else top_panel_data
        top_panel_data_interpolated = interpolate.griddata((radius, z), top_panel_data_to_interpolate, (R_plot, Z_plot),
                                                           **top_interpolation_kwargs)

        if top_interpolate_logarithms:
            top_panel_data_interpolated = np.power(10, top_panel_data_interpolated)
        drawing_top = axes.pcolormesh(
            R_plot, Z_plot,
            top_panel_data_interpolated, norm=normalizer, rasterized=True, **top_image_kwargs
        )

        if top_panel_levels is not None:
            top_panel_levels_data_to_interpolate = np.log10(
                top_panel_data) if top_interpolate_logarithms else top_panel_levels_data
            top_panel_levels_data_interpolated = interpolate.griddata((radius, z), top_panel_levels_data_to_interpolate,
                                                                      (R_plot, Z_plot),
                                                                      **top_interpolation_kwargs)
            if top_interpolate_logarithms:
                top_panel_levels_data_interpolated = np.power(10, top_panel_levels_data_interpolated)

            contours_top = axes.contour(R_plot, Z_plot, top_panel_levels_data_interpolated, norm=normalizer,
                                        levels=top_panel_levels, **top_contours_kwargs)
    if bottom_panel_data is not None:
        try:
            bottom_panel_name = get_name(bottom_panel_data.name)
        except AttributeError:
            bottom_panel_name = f"Undefined {type(bottom_panel_data)}"

        label_bottom = r'{:g} $\times$ {}'.format(
            bottom_data_multiplier, bottom_panel_name
        ) if bottom_data_multiplier != 1 \
            else bottom_panel_name
        txt = axes.text(0.05, 0.05, label_bottom,
                        transform=axes.transAxes,
                        verticalalignment='bottom',
                        **text_kwargs)  # , color='white')
        # if False: txt.set_path_effects([matplotlib.patheffects.withStroke(linewidth=1, foreground='w')])
        axes.axhline(0, color=zerolinecolor, label=None)
        if bottom_interpolate is None:
            drawing_bottom = axes.tripcolor(
                radius, -z, bottom_data_multiplier * bottom_panel_data, norm=normalizer, shading=shading,
                # rasterized=True,
                **bottom_image_kwargs
            )
            if bottom_panel_levels is not None:
                contours_bottom = axes.tricontour(
                    radius, -z, bottom_data_multiplier * bottom_panel_levels_data, norm=normalizer,
                    levels=bottom_panel_levels,
                    **bottom_contours_kwargs
                )
        else:
            r_plot = np.logspace(math.log10(min(radius)), math.log10(max(radius)), bottom_interpolate)
            z_plot = np.linspace(min(z), max(z), bottom_interpolate)

            R_plot, Z_plot = np.meshgrid(r_plot, z_plot)

            bottom_panel_data_to_interpolate = np.log10(
                bottom_panel_data) if bottom_interpolate_logarithms else bottom_panel_data
            bottom_panel_data_interpolated = interpolate.griddata((radius, z), bottom_panel_data_to_interpolate,
                                                                  (R_plot, Z_plot),
                                                                  **bottom_interpolation_kwargs)
            if bottom_interpolate_logarithms:
                bottom_panel_data_interpolated = np.power(10, bottom_panel_data_interpolated)
            drawing_bottom = axes.pcolormesh(
                R_plot, -Z_plot,
                bottom_data_multiplier * bottom_panel_data_interpolated, norm=normalizer, rasterized=True,
                **top_image_kwargs
            )
            if bottom_panel_levels is not None:
                bottom_panel_levels_data_to_interpolate = np.log10(
                    bottom_panel_levels_data) if bottom_interpolate_logarithms else bottom_panel_levels_data
                bottom_panel_levels_data_interpolated = interpolate.griddata((radius, z),
                                                                             bottom_panel_levels_data_to_interpolate,
                                                                             (R_plot, Z_plot),
                                                                             **bottom_interpolation_kwargs)
                if bottom_interpolate_logarithms:
                    bottom_panel_levels_data_interpolated = np.power(10, bottom_panel_levels_data_interpolated)
                contours_bottom = axes.contour(R_plot, -Z_plot,
                                               bottom_data_multiplier * bottom_panel_levels_data_interpolated,
                                               norm=normalizer,
                                               levels=bottom_panel_levels, **bottom_contours_kwargs)

    try:
        top_panel_name = get_name(top_panel_data.name)
    except AttributeError:
        top_panel_name = f"Undefined {type(top_panel_data)}"
    txt = axes.text(0.05, 0.95, top_panel_name, transform=axes.transAxes,
                    verticalalignment='top', **text_kwargs)  # s, color='white')
    # if False:
    #     txt.set_path_effects([matplotlib.patheffects.withStroke(linewidth=1, foreground='w')])

    if highlight:
        axes.axvspan(*highlight, **highlight_kwargs)
    if xlim:
        axes.set_xlim(xlim)
    if ylim:
        axes.set_ylim(ylim)

    if no_negatives_in_labels:
        try:
            formatter = FuncFormatter(lambda x, pos: f'{abs(x):.2f}')  # todo as function def
            axes.yaxis.set_major_formatter(formatter)
        except ValueError:
            module_logger.warning("Could not fix yticks for negatives: %s", axes.get_yticklabels())
    if do_colorbar:
        if under_color is None or ('extend' in cbar_kwargs.keys()):
            colorbar = axes.figure.colorbar(
                drawing_top, label=colorbar_label, ticks=colorbar_ticks, ax=axes, **cbar_kwargs
            )
        else:
            colorbar = axes.figure.colorbar(
                drawing_top, label=colorbar_label, ticks=colorbar_ticks, ax=axes, extend='min', **cbar_kwargs
            )
        if colorbar_ticklabels:
            if colorbar_ticklabels_rotate:
                colorbar.ax.set_yticklabels(
                    colorbar_ticklabels,
                    rotation=colorbar_ticklabels_rotate, rotation_mode='anchor'
                )
            else:
                colorbar.ax.set_yticklabels(
                    colorbar_ticklabels
                )
        if top_panel_levels is not None:
            try:
                colorbar.add_lines(contours_top, erase=False)
            except Exception:
                pass
        if bottom_panel_levels is not None:
            try:
                colorbar.add_lines(contours_bottom, erase=False)
            except Exception:
                pass
    return axes.figure
