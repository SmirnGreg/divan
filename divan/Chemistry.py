import logging
import collections
from warnings import warn

import astropy.table
import numpy as np
from matplotlib import colors, pyplot as plt, cm

from divan.plot_functions import generate_2d_disk_figure
from divan.names import get_name
from divan._DivanBase import DivanBase
from divan.names import get_name

module_logger = logging.getLogger("divan.2d.chemistry")


class Chemistry(DivanBase):
    """
    MixIn class for chemistry calculations and visualization
    """

    def normalize_chemical_structure(
            self,
            normalize_to='<H>'  # '<H>', species or value
    ) -> astropy.table.table.Table:
        """
        Method to normalize self.chemical_structure to selected species abundance -- from [cm-3] to relative abundances
        :param normalize_to: value, astropy.table.Column, string with name of corresponding species, or '<H>' for 'H2'+'H2+'+'H'+'H+'+'H-'
        :return: astropy table with normalized chemical_structure
        """
        if normalize_to == '<H>':
            value_normalize = (
                    2 * self.chemical_structure['H2'] + 2 * self.chemical_structure['H2+'] +
                    + self.chemical_structure['H'] + self.chemical_structure['H+'] + self.chemical_structure['H-']
            )
            value_normalize.name = r'$<\mbox{H}>$'
        elif normalize_to in self.chemical_structure.keys():
            value_normalize = self.chemical_structure[normalize_to] + 0
        else:
            value_normalize = normalize_to

        chemical_structure = self.chemical_structure
        columns_to_change = chemical_structure.columns[2:]
        for column in columns_to_change:
            if column == 'Most abundant species':
                continue
            chemical_structure[column][:] = chemical_structure[column] / value_normalize

        module_logger.info(
            'Chemical structure normalized to: \n {} \n taken from {}'.format(value_normalize, normalize_to))
        self.chemical_structure = chemical_structure
        self.chemistry_normalize_to = value_normalize.name
        return self.chemical_structure

    def generate_figure_chemistry(
            self,
            r=None, z=None,
            spec1=None, spec2=None,
            do_interpolate=None,  # Points to interpolate, no interpolation if False
            interpolation_kwargs=None,
            contours_kwargs=None,
            image_kwargs=None,
            levels=None,
            normalizer=colors.LogNorm(vmin=1e-7, vmax=1e-2),
            axes=None,
            title='Species number density',
            colorbar_label=None,
            **kwargs  # To be passed to generate_2d_disk_figure
    ):
        """
        This method returns figure with generated image, replaces self.figure_chemistry with it and appends it to self.figures list.

        :param r:
        :param z:
        :param spec1:
        :param spec2:
        :param do_interpolate:
        :param interpolation_kwargs:
        :param contours_kwargs:
        :param image_kwargs:
        :param levels:
        :param normalizer:
        :param axes:
        :param title:
        :param kwargs:
        :return:
        """
        interpolation_kwargs = interpolation_kwargs or {}
        contours_kwargs = contours_kwargs or {}
        image_kwargs = image_kwargs or {}

        module_logger.info(f"Generating figure of chemistry distribution: species {get_name(spec1)}, {get_name(spec2)}")
        if 'cmap' not in image_kwargs.keys():
            image_kwargs['cmap'] = 'PuBuGn'
        levels_sorted = sorted(levels) if levels else None
        if (r is None) or (z is None):
            try:
                if 'Height to radius' in self.chemical_structure.colnames:
                    zrcolname = 'Height to radius'
                else:
                    zrcolname = 'Relative height'
                r, z = (
                    self.chemical_structure['Radius'],
                    self.chemical_structure[zrcolname],
                )
                module_logger.debug("Parameters successfully taken from self")
            except:  # TODO make better exception
                raise AttributeError(
                    'No coordinates data given for .generate_figure_chemistry(). Use .read_chemical_structure() or send arguments')
        if type(spec1) in (str, np.str, np.str_):
            try:
                spec1 = (self.chemical_structure[spec1])
            except KeyError:
                spec1 = None
            except:
                raise AttributeError(
                    'No spec1 data given for .generate_figure_chemistry(). Use .read_chemical_structure() or send arguments')
        if type(spec2) in (str, np.str, np.str_):
            try:
                spec2 = (self.chemical_structure[spec2])
            except KeyError:
                warn("No data found for spec2 {}, spec1 used instead!".format(spec2), UserWarning)
                spec2 = spec1
                # spec2 = None
            except:
                raise AttributeError(
                    'No spec2 data given for .generate_figure_chemistry(). Use .read_chemical_structure() or send arguments')
        if axes is None:
            self.figure_chemistry, axes = plt.subplots()
            self.figures.append(self.figure_chemistry)
            module_logger.debug('Generate axes')
        if self.titles: axes.set_title(title)
        axes.set_xlabel('Radius [au]')
        axes.set_ylabel('Height/Radius')

        if colorbar_label is None:
            colorbar_label = r'Abundance to {}'.format(
                get_name(self.chemistry_normalize_to)) if self.chemistry_normalize_to \
                else r'Number density [cm$^{-3}$]'
        fig = generate_2d_disk_figure(
            r, z, top_panel_data=spec1, top_panel_levels=levels_sorted,
            bottom_panel_data=spec2,
            top_image_kwargs=image_kwargs,
            top_interpolate=do_interpolate, axes=axes, colorbar_label=colorbar_label,
            top_interpolation_kwargs=interpolation_kwargs, top_contours_kwargs=contours_kwargs,
            top_interpolate_logarithms=True, height_is_relative=True, normalizer=normalizer, **kwargs
        )
        return fig

    def generate_most_abundant_species(
            self,
            species='all',  # 'all' or tuple of species names
            threshold=1e-15,
            threshold_on_absolute=False,  # use initial chemical structure if true
    ):
        """
        Calculate the most abundant species in chemical structure for every data row

        :param species:
        :param threshold:
        :param threshold_on_absolute:
        :return:

        """
        if threshold_on_absolute:
            table_use = self.chemical_structure_initial
        else:
            table_use = self.chemical_structure
        if species == 'all':
            subtable = astropy.table.Table(table_use.columns[3:])
        else:
            subtable = table_use[species]
        self.chemical_structure['Most abundant species'] = None
        for i, row in enumerate(subtable):
            row_ndarray = np.array([value for value in row])
            most_abundant_column_number = np.argmax(row_ndarray)
            most_abundant_species_name = row.colnames[most_abundant_column_number]
            most_abundant_number_density = row[most_abundant_column_number]
            if most_abundant_number_density > threshold:
                self.chemical_structure['Most abundant species'][i] = most_abundant_species_name
            else:
                self.chemical_structure['Most abundant species'][i] = ''
        self.tuple_of_species_for_most_abundant = species
        self.threshold_most_abundant_species = threshold
        self.threshold_most_abundant_species_on_absolute = threshold_on_absolute
        return self.chemical_structure['Most abundant species']

    def generate_figure_most_abundant_species(
            self,
            r=None, z=None,
            most_abundant=None,
            image_kwargs=None,
            do_interpolate=100,
            interpolation_kwargs=None,
            colors_for_species=(),  # An iterable with any valid matplotlib color
            species=None,
            **most_abundant_kwargs
    ):
        """
        This method returns figure with generated image, replaces self.figure_most_abundant_species with it and appends it to self.figures list.

        :param r:
        :param z:
        :param most_abundant:
        :param image_kwargs:
        :param do_interpolate:
        :param interpolation_kwargs:
        :param colors_for_species:
        :param species:
        :param most_abundant_kwargs:
        :return:
        """
        if species is not None:
            try:
                self.generate_most_abundant_species(species, **most_abundant_kwargs)
            except:
                warn("Could not generate most abundant species map for {species}".format(species=species), UserWarning)
        interpolation_kwargs = interpolation_kwargs or {}
        image_kwargs = image_kwargs or {}

        tab10 = cm.get_cmap('tab10')
        colors_for_species = colors_for_species or tuple(map(tab10, np.linspace(0, 1, 10)))

        if (r is None) or (z is None):
            try:
                r, z = (
                    self.chemical_structure['Radius'],
                    self.chemical_structure['Relative Height'],
                )
            except:
                raise AttributeError(
                    'No coordinates given for .generate_figure_most_abundant_species(). '
                    'Use .read_chemical_structure() or send arguments'
                )
        if most_abundant is None:
            try:
                most_abundant = self.chemical_structure['Most abundant species']
            except:
                raise AttributeError(
                    'No data given for .generate_figure_most_abundant_species(). '
                    'Use .read_chemical_structure() AND .generate_figure_most_abundant_species() or send arguments'
                )
        self.figure_most_abundant_species, ax_most_abundant_species = plt.subplots()
        if self.titles:
            ax_most_abundant_species.set_title(
                'Most abundant species of {}'.format(self.tuple_of_species_for_most_abundant))
        ax_most_abundant_species.set_xlabel('Radius [au]')
        ax_most_abundant_species.set_ylabel('Height/Radius')
        ax_most_abundant_species.semilogx()
        ax_most_abundant_species.autoscale(enable=True, axis='x', tight=True)
        ax_most_abundant_species.autoscale(enable=True, axis='y', tight=True)

        if self.threshold_most_abundant_species_on_absolute:
            quan = r'n'
            dim = r'\ \mbox{cm}^{-3}'
        else:
            quan = r'n/n_{\mbox{\scriptsize{%s}}}' % self.chemistry_normalize_to
            dim = ''
        most_abundant_odict = collections.OrderedDict(collections.Counter(most_abundant).most_common(10))
        most_abundant_value = astropy.table.Column(
            length=len(most_abundant), dtype='int64',
            name=r'$' + quan + '>' + str(self.threshold_most_abundant_species) + dim + r'$'
        )
        most_abundant_value[np.where(most_abundant == '')] = -1
        if '' in most_abundant_odict.keys():
            del (most_abundant_odict[''])
        for i, entry in enumerate(most_abundant_odict):
            most_abundant_value[np.where(most_abundant == entry)] = i

        cmap = colors.ListedColormap(colors_for_species[0:len(most_abundant_odict)])

        if 'cmap' not in image_kwargs.keys():
            cmap.set_under('w')
            image_kwargs['cmap'] = cmap  # 'tab10'

        self.figure_most_abundant_species = generate_2d_disk_figure(
            r, z, top_panel_data=most_abundant_value,
            top_image_kwargs=image_kwargs,
            top_interpolation_kwargs=interpolation_kwargs,
            top_interpolate=do_interpolate, axes=ax_most_abundant_species, colorbar_label=r'Species',
            normalizer=colors.Normalize(vmin=-0.5, vmax=len(most_abundant_odict) - 0.5),
            colorbar_ticks=np.arange(len(most_abundant_odict)), colorbar_ticklabels=most_abundant_odict.keys(),
            height_is_relative=True,
        )
        self.figures.append(self.figure_most_abundant_species)

        return self.figure_most_abundant_species

    def basic_chemistry_visualization(
            self,
            first_plot_species=('H', 'H+', 'H2', 'He'),
            normalizer=None,
            image_kwargs=None,
            subplots_kwargs=None,
            ices=True,
            suptitle=None,
            **kwargs
    ):
        """
        Plot 4 species (optionally, with their ices).

        This method returns figure with generated image, replaces self.figure_basic_chemistry_visualization_1 with it and appends it to self.figures list.

        :param first_plot_species:
        :param normalizer:
        :param image_kwargs:
        :param subplots_kwargs:
        :param ices:
        :param suptitle:
        :param kwargs:
        :return:
        """
        if suptitle is None:
            if self.chemistry_normalize_to is None:
                suptitle = 'Absolute abundances'
            else:
                suptitle = 'Relative abundances'
        subplots_kwargs = subplots_kwargs or {}
        shape = (2, 2)
        self.figure_basic_chemistry_visualization_1, ax_basic_chemistry_visualization_1 = plt.subplots(
            *shape, **subplots_kwargs  # gridspec_kw={'wspace': 0.1, 'hspace': 0.1}
        )
        if suptitle: self.figure_basic_chemistry_visualization_1.suptitle(suptitle)
        # gridspec = matplotlib.gridspec.GridSpec
        self.figures.append(self.figure_basic_chemistry_visualization_1)
        for i, ax in enumerate(ax_basic_chemistry_visualization_1.reshape(-1)):
            if ices:
                spec2 = 'g' + first_plot_species[i]
            else:
                spec2 = first_plot_species[i]
            self.generate_figure_chemistry(
                spec1=first_plot_species[i], spec2=spec2,
                do_interpolate=100, interpolation_kwargs={'method': 'linear'},
                normalizer=normalizer,
                image_kwargs=image_kwargs,
                axes=ax, **kwargs,  # do_colorbar=False
            )
            pos = np.unravel_index(i, shape)
            if pos[0] != shape[0] - 1:
                ax.set_xticklabels([])
                ax.set_xlabel('')
            if pos[1] != 0:
                ax.set_yticklabels([])
                ax.set_ylabel('')
            if pos[0] == shape[0] - 1:
                # ax.xaxis.set_major_locator(matplotlib.ticker.LogLocator(prune='upper'))
                # labels = ax.get_xticklabels()
                # del(labels[-1])# = ''
                # ax.set_xticklabels(labels)
                del (ax.axes.xaxis.majorTicks[-1])
            ax.set_title('')
        return self.figure_basic_chemistry_visualization_1

    def generate_figure_most_total_abundant(
            self, Npages=2, from_start=False, *args, **kwargs
    ):
        """
        Method to generate Npages with 4 plots each with divan.basic_chemistry_visualization. The plots are sorted in order of total mass in the disk. The second call of the same function will continue by default.

        :param Npages: number of pages to plot
        :param from_start: set to True to start from the beginning
        :param args: to be passed to Divan.basic_chemistry_visualization
        :param kwargs: to be passed to Divan.basic_chemistry_visualization
        """
        if from_start:
            self.skip_pages_most_total_abundant = 0
        for page in range(Npages):
            spec = self.total_abundances['Species name'][
                   4 * (page + self.skip_pages_most_total_abundant):
                   4 * (page + self.skip_pages_most_total_abundant + 1)
                   ]
            self.basic_chemistry_visualization(
                first_plot_species=spec, ices=False, *args, **kwargs
            )
        self.skip_pages_most_total_abundant += Npages
