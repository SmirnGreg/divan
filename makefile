# Please note that makefile is not a common way to install python package!
# Use
# $ python setup.py -h
# for more information!

install:
	python setup.py install
	echo Please note that makefile is not a common way to install python package! Use $ python setup.py -h for help
