# Import Divan class, the main interface for reading the data and saving figures
from divan import Divan

# Import other packages
import matplotlib
import datetime

# The code below will not run if
# >>> import simple_demo
# is done (standard in Python)
if __name__ == '__main__':
    # Create an instance of Divan class named andes2out
    andes2out = Divan(
        input_folder="../data_output_gtb/",  # set attribute input_folder to "../data_output_gtb"
        verbosity=2,  # set high verbosity
    )
    # No actual data reading was performed yet

    # Now read structures from input_folder location and store in attributes of andes2out
    andes2out.read_surface_densities()
    andes2out.read_physical_structure()
    andes2out.read_chemical_structure()
    andes2out.read_chemitry_totalmass()
    andes2out.read_gtb()
    # Data is read

    # Generate text page with LaTeX support
    andes2out.generate_text_page(
        # suptitle="Introduction",  # Page title
        # text_to_write=fr"""
        # ANDES2 output \\
        # Generated at {datetime.datetime.now()} \\
        # from {andes2out.input_folder} \\
        # """ + r"""
        # \begin{tabular}{l|r}
        # 1 & 2 \\
        # 3 & 4 \\
        # \end{tabular}
        # """,  # LaTeX to write on page
    )

    # Generate figure with surface densities
    andes2out.generate_figure_surface_densities()

    ## Uncomment arguments and re-run to see what they make
    ## some of arguments are method-specific they are marked with ↓
    ## some are passed to divan.plot_functions.generate_2d_disk_figure, they are marked with ⇉

    # Generate figure with volume densities
    andes2out.generate_figure_volume_densities(
        ## Method-specific keywords:
        # extra_gas_to_dust=100,  # ↓ multiply dust panel for comparison
        # levels=[1e-16, 1e-14],  # ⇉ plot contours
        # image_kwargs={'cmap': 'viridis_r'},  # ⇉ change colormap by setting image_kwargs['cmap'] = 'viridis_r'
        # do_interpolate=100, interpolation_kwargs={'method': 'linear'}, # ⇉ Linearly interpolate data on 100x100 grid
        # do_interpolate=200, interpolation_kwargs={'method': 'nearest'},  # ⇉ Fill with nearest value on 200x200 grid
    )

    # Generate figure with temperatures
    andes2out.generate_figure_temperatures()

    # Generate figures with most abundant (by mass) species
    andes2out.generate_figure_most_total_abundant(
        # Npages=10,  # ↓ Number of pages to print (2 by default)
        # image_kwargs={'cmap': 'nipy_spectral'},  # ⇉ change colormap by setting image_kwargs['cmap'] = 'nipy_spectral'
        # normalizer=matplotlib.colors.LogNorm(vmin=1e0, vmax=1e10),  # ⇉ instance of matplotlib.colors.Normalizer or another callable that converts values to 0-1 range
        # under_color='w',  # ⇉ color for values less than normalizer.vmin
        # bottom_data_multiplier=1e10,  # ↓ multipy bottom data by this value for higher range
        # suptitle="Absolute abundances"  # ↓ title of the figure
    )
    # Call it again to continue
    andes2out.generate_figure_most_total_abundant()

    # Reverse back to start
    andes2out.skip_pages_most_total_abundant = 0

    # Normalize chemical structure by dividing by column with number densities of given species
    andes2out.normalize_chemical_structure(
        # normalize_to='gH2O',  # By default, to all H atoms
    )
    andes2out.generate_figure_most_total_abundant(
        # Npages=10,  # ↓ Number of pages to print (2 by default)
        # image_kwargs={'cmap': 'nipy_spectral'},  # ⇉ change colormap by setting image_kwargs['cmap'] = 'nipy_spectral'
        # normalizer=matplotlib.colors.LogNorm(vmin=1e0, vmax=1e10),  # ⇉ instance of matplotlib.colors.Normalizer or another callable that converts values to 0-1 range
        # under_color='w',  # ⇉ color for values less than normalizer.vmin
        # bottom_data_multiplier=1e10,  # ↓ multipy bottom data by this value for higher range
        # suptitle="Absolute abundances"  # ↓ title of the figure
    )

    # Explicit chemistry figure generation
    andes2out.generate_figure_chemistry(
        spec1="H2O",  # ↓
        spec2="CO2",  # ↓
    )

    # Basic visualization of 4 species, with their ices by default
    andes2out.basic_chemistry_visualization(
        # first_plot_species=('H2O', 'CO2', 'CO', 'OH'),  # ↓
        # ices=False,  # ↓ True is default, try to find ice for this species
    )

    # Generate a Column with name of most abundant (by number density) species in bin
    andes2out.generate_most_abundant_species(
        species=('H2O', 'CO2', 'O', 'O2', 'O+'),  # ↓
    )
    # Generate a figure of most abundant species
    andes2out.generate_figure_most_abundant_species()

    # Generate GTB temperature plot
    andes2out.generate_figure_gtb(
        # levels=[0.9, 2, 4, 8, 16, 32, 64]
    )

    # Overplot contours on top of the last figure
    andes2out.add_extra_contours(
        # axes=andes2out.figures[-2].axes[0],  # ↓ Overplot by default on first [0] axes of last [-1] figure, can be selected explicit
        contour_data_columnname='AV_true',  # ↓  Columnname to make contours
        levels=[1, 10, 100],  # ⇉
        # contour_kwargs={'colors': 'black'},  # ⇉
        # clabel=r'$A_V = %d$',  # ⇉ c-format string to label contours
        # do_interpolate=100, interpolation_kwargs={'method': 'cubic'},  # ⇉ interpolation
        both_panels=False,  # ↓ do not plot on both top and bottom panels
    )

    # Generate most active coolant and heater
    andes2out.generate_figure_most_active_coolant_or_heater(
        heaters=False, # ↓ Plot heaters instead of coolants if True
        # colorbar_ticklabels_rotate=30, # ⇉ for long coolants name
    )
    andes2out.generate_figure_most_active_coolant_or_heater(
        heaters=True,  # ↓ Plot heaters instead of coolants if True
    )

    # Save produced figures to PDF
    andes2out.save_figures_pdf("simple.pdf")
    pass
