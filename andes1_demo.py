import divan
import numpy as np
from matplotlib.colors import LogNorm, Normalize
from copy import copy
import logging


if __name__ == '__main__':
    # fnames = ["new_notevolved_Av", "new_evolved", "old_evolved", "new_notevolved_fullchem_fix", "new_evolved_fullchem",
    #           "new_notevolved_gap_Av", "new_notevolved_deepgap_Av"]
    fnames = ["new_notevolved_Av", "new_notevolved_gap_Av", "new_notevolved_deepgap_Av", "benchmark"]
    divans = []
    andes2 = {3}
    image_kwargs_comparison = {'cmap': 'coolwarm'}
    for i, fname in enumerate(fnames):
        if i in andes2:
            dvn = divan.Divan(
                input_format='ANDES2',
                input_folder=fr'../data_output_{fname}/')  # , input_folder='../../andes/AATau4rad_ad50d0e__AATau4rad/completed/')
            # divan.read_physical_structure(data_file='../../andes/AATau4rad_ad50d0e__AATau4rad/completed/Structure_m.dat')
            dvn.read_physical_structure()
        else:
            dvn = divan.Divan(
                input_format='ANDES1')  # , input_folder='../../andes/AATau4rad_ad50d0e__AATau4rad/completed/')
            # divan.read_physical_structure(data_file='../../andes/AATau4rad_ad50d0e__AATau4rad/completed/Structure_m.dat')
            dvn.read_physical_structure(data_file=f'andes1_output/{fname}.dat')

        dvn.generate_figure_volume_densities(
            # do_interpolate=100, interpolation_kwargs={'method': 'linear'},
            extra_gas_to_dust=100,
            levels=[1e-11, 1e-12, 1e-13, 1e-14, 1e-15, 1e-16, 1e-17, 1e-18],
            normalizer=LogNorm(vmin=1e-20, vmax=1e-12),
            no_negatives_in_labels=False
        )
        dvn.figures[-1].axes[0].set_xlim(10, 500)
        dvn.figures[-1].axes[0].set_ylim(-1, 1)

        dvn.generate_figure_temperatures(
            # do_interpolate=100, interpolation_kwargs={'method': 'linear'},
            normalizer=LogNorm(vmin=1e1, vmax=2e4),
            levels=[20, 40, 100, 300, 1000],
            no_negatives_in_labels=False
        )
        dvn.figures[-1].axes[0].set_xlim(10, 500)
        dvn.figures[-1].axes[0].set_ylim(-1, 1)

        dvn.save_figures_pdf(f'andes1_output/{fname}.pdf')

        divans.append(dvn)

    # comparison of pairs
    # pairs = ((0, 3), (1, 4), (2, 1), (5, 0), (6,0), (6,5))
    pairs = ((1, 0), (2, 0), (2, 1), (3, 0),)
    for pair in pairs:
        dvn1, dvn2 = divans[pair[0]], divans[pair[1]]

        dvn = copy(dvn1)
        dvn.figures = []

        phys1 = divan.TablePlus(dvn1.physical_structure)
        phys2 = divan.TablePlus(dvn2.physical_structure).interpolate(phys1)

        dvn.physical_structure = phys1 / phys2
        for column in dvn.physical_structure.columns:
            dvn.physical_structure[column][np.where(np.isnan(dvn.physical_structure[column]))] = 1
        dvn.generate_figure_volume_densities(
            # do_interpolate=100, interpolation_kwargs={'method': 'linear'},
            extra_gas_to_dust=1,
            levels=[1. / 3, 1. / 1.1, 1.1, 3.],
            normalizer=LogNorm(vmin=0.1, vmax=10.),
            no_negatives_in_labels=False,
            image_kwargs=image_kwargs_comparison,
            colorbar_label=r'Volume density ratio',
        )
        dvn.figures[-1].axes[0].set_xlim(10, 500)
        dvn.figures[-1].axes[0].set_ylim(-1, 1)

        dvn.generate_figure_temperatures(
            # do_interpolate=100, interpolation_kwargs={'method': 'linear'},
            levels=[1. / 3, 1. / 1.1, 1.1, 3.],
            normalizer=LogNorm(vmin=0.1, vmax=10.),
            no_negatives_in_labels=False,
            image_kwargs=image_kwargs_comparison,
            colorbar_label=r'Temperature ratio'
        )
        dvn.figures[-1].axes[0].set_xlim(10, 500)
        dvn.figures[-1].axes[0].set_ylim(-1, 1)

        try:
            dvn1.add_extra_contours(
                dvn.figures[-1].axes[0], contour_data_columnname='AV_true',
                levels=[1, 3, 10],
                clabel="$A_V = %d$"
            )
        except KeyError:
            logging.error("Av not found")


        dvn.save_figures_pdf(f'andes1_output/comp_{fnames[pair[0]]}_{fnames[pair[1]]}.pdf')
